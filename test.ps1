$config_dir = Join-Path $HOME "AppData\Local\umomwd\umomwd"
$cache_dir  = Join-Path $HOME "AppData\Local\umomwd\umomwd\Cache"


Write-Output "Config Directory: $config_dir"
Write-Output "Cache Directory: $cache_dir"

# Function to test commands
function Test-UMO {
    param (
        [string]$testName,
        [string]$command
    )

    Invoke-Expression "$command" > ./testoutput 2>&1
    if ($LASTEXITCODE -ne 0) {
        Write-Error "NOT OK: $testName (Exit code: $LASTEXITCODE)"
        type ./testoutput
        exit $LASTEXITCODE  # Fail with the command's exit code
    } else {
        Write-Output "OK: $testName"
    }
}

# Create directories if they don't exist
New-Item -ItemType Directory -Path $config_dir -Force | Out-Null
New-Item -ItemType Directory -Path $cache_dir -Force | Out-Null

# Create a config file if it doesn't exist
$configFilePath = Join-Path $config_dir "config.json"
if (-Not (Test-Path $configFilePath)) {
    '{"NEXUS_API_KEY": "...", "TES3CMD": "tes3cmd.exe", "MORROWIND_DIR": ".", "BASEPATH": "OpenMWMods"}' | Set-Content -Path $configFilePath
}

# List contents of directories
Get-ChildItem -Path $config_dir
Get-ChildItem -Path $cache_dir

# Simulate 'tes3cmd' presence
New-Item -ItemType File -Path "tes3cmd.exe" -Force | Out-Null
Set-ItemProperty -Path "tes3cmd.exe" -Name IsReadOnly -Value $false

# Run UMO tests
Test-UMO "cache-sync" "C:\Python312\python.exe src\umo.py cache sync expanded-vanilla -S MOMWGameplay"
Test-UMO "install" "C:\Python312\python.exe src\umo.py install --download-only --no-gui expanded-vanilla -S MOMWGameplay"
Test-UMO "install-check-archive" "dir ${cache_dir}\archive-cache\MOMWGameplay\ | grep '.archive'"
Test-UMO "install" "C:\Python312\python.exe src\umo.py install --no-gui expanded-vanilla -S MOMWGameplay"
Test-UMO "install-check-extracted" "dir OpenMWMods\expanded-vanilla\Gameplay\MOMWGameplay\ | grep 'momw-gameplay-ui.omwscripts'"
Test-UMO "cache-query" "C:\Python312\python.exe src\umo.py cache query momw expanded-vanilla.mods_by_name.MOMWGameplay | grep 'MOMWGameplay'"
Test-UMO "cache-dump" "C:\Python312\python.exe src\umo.py cache dump expanded-vanilla | grep 'MOMWGameplay'"

# Handle cache dump and addition
C:\Python312\python.exe src\umo.py cache dump expanded-vanilla > expanded-vanilla-patched
type expanded-vanilla-patched
C:\Python312\python.exe src\umo.py cache add expanded-vanilla-patched
Remove-Item -Path "expanded-vanilla-patched"

Test-UMO "cache-add" "C:\Python312\python.exe src\umo.py cache query momw expanded-vanilla-patched.mods_by_name.ProjectAtlas | grep 'ProjectAtlas'"
