#!/bin/bash

dyt() {
    ori="textures_orig"
    dest="textures"

    txt="$1"
    b=$(basename "$txt" .txt)
    oa=${b}_occ.dds
    spec=${b}_spec.dds
    alb=${b}_alb.dds

    # if [ -f "$ori/$alb" ]; then
    #     cp "$ori/$alb" "$(echo "$dest/$alb" | sed 's/_alb//g')"
    # fi

    param=$(grep ParametersTexture "$txt" | grep dds | sed 's/ParametersTexture=//g' | sed 's/\\/\//g' | sed 's/textures/textures_orig/g' | sed -e 's/\(.*\)/\L\1/' | tr -d '\r\n')
    roughness=$(grep RoughnessSource "$txt" | grep dds | sed 's/RoughnessSource=//g' | sed 's/\\/\//g' | sed 's/textures/textures_orig/g' | sed -e 's/\(.*\)/\L\1/' | tr -d '\r\n')
    occ=$(grep SpecOcclusionSource= "$txt" | grep dds | sed 's/SpecOcclusionSource=//g' | sed 's/\\/\//g' | sed 's/textures/textures_orig/g' | sed -e 's/\(.*\)/\L\1/' | tr -d '\r\n')
    metal=$(grep FresnelSource "$txt" | grep dds | sed 's/FresnelSource=//g' | sed 's/\\/\//g' | sed -e 's/\(.*\)/\L\1/' | tr -d '\r\n')
    roughness_multi=$(grep Roughness= "$txt" | sed 's/Roughness=//g' | sed 's/\\/\//g' | sed 's/,/./g' | tr -d '\r\n' )
    metal_multi=$(grep FresnelReflectance= "$txt" | sed 's/FresnelReflectance=//g' | sed 's/\\/\//g' | sed 's/,/./g' | tr -d '\r\n')

    if [ -n $roughness ]
    then
        roughness=$param
    fi

    if [ -n $metal ]
    then
        metal=$param
    fi

    if [ -n $occ ]
    then
        if [ ! -f "$ori/$oa" ]
        then
            occ=$param
        else
            occ=$ori/$oa
        fi
    else
        if [ ! -f $occ ]
        then
            if [ ! -f "$ori/$oa" ]
            then
                occ=$param
            else
                occ=$ori/$oa
            fi
        fi
    fi


    if [ -f $param ]
    then
        echo doing $txt $roughness_multi $metal_multi
        magick \( "$metal" -channel r -fx $metal_multi -channel r -separate +channel \) \
               \( "$roughness"  -channel g -fx $roughness_multi -channel g -negate -channel g -separate +channel \) \
               \( "$occ" -channel b -separate +channel \) \
               -set colorspace sRGB -combine -opaque black "$dest/$spec"
    else
        echo "NO PARAM TEXTURE for $txt : $param"
    fi
}

export -f dyt

find materials -type f -name "*.txt" -print0 | xargs -0 -I {} -P "$(nproc)" bash -c 'dyt "{}"'
exit 0
