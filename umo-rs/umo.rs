#[cfg(not(windows))]
compile_error!("This build of umo is only for Windows!");

use std::{
    env::{args as startup_args, current_exe, var},
    fmt::Debug,
    fs::{create_dir_all, metadata, remove_dir_all, remove_file, File},
    io::{copy, Error, ErrorKind, Read},
    path::{PathBuf, MAIN_SEPARATOR},
    process::{exit, Command, ExitCode},
    time::Duration,
};

use bytes::Bytes;
use minisign_verify::{PublicKey, Signature};
use reqwest::blocking::{Client, ClientBuilder, Response};
use serde_json::{from_str as json_from_str, Value as JSONValue};
use zip::{read::ZipFile, ZipArchive};

const DOWNLOAD_ZIP: &str = "umo-win.zip";
const THE_ZIP: &str = "temp.zip";

const MAX_DOWNLOAD_ATTEMPTS: u8 = 5;
const PUB_KEY: &str = include_str!("../minisign.pub");
const REQUIRED_FILES: [(&str, &str); 3] = [
    ("python.exe", "You appear to be missing python.exe!"),
    ("umo.py", "Unable to locate umo main file!"),
    ("version.txt", "Unable to locate umo version file!"),
];

const UMO_LATEST_PACKAGE_URL: &str = "https://gitlab.com/api/v4/projects/modding-openmw%2Fumo/packages?package_name=umo&sort=desc&per_page=1";
const UMO_PACKAGE_FILES_URL: &str =
    "https://gitlab.com/modding-openmw/umo/-/package_files/{}/download";

// These essentially should be a lib.rs but we ain't doing that

/// Abandon the extraction process, emitting the error to stderr,
/// deleting the embedded python distribution and temporary zip,
/// and exiting with code 127
fn abandon_extract(err: &str) {
    eprintln!("{}", err);
    let _ = remove_dir_all(get_python_path());
    let _ = remove_file(
        get_python_path()
            .parent()
            .expect("Failed to get umo root directory!")
            .join(THE_ZIP),
    );
    exit(127);
}

/// Small helper to create parent directories for archives if they don't exist
fn create_path_parent(path: &PathBuf) {
    if let Some(file_parent) = path.parent() {
        if let Err(error) = create_dir_all(file_parent) {
            abandon_extract(&format!(
                "Failed to create archive file parent directory {}!",
                error
            ));
        }
    }
}

/// Extracts the entire archive to the target directory, terminating the app on most failures
fn extract_archive() -> Result<(), Error> {
    let zip_path = get_python_path()
        .parent()
        .expect("Failed to get archive parent directory!")
        .join(THE_ZIP);

    let zip_file = File::open(&zip_path)?;
    let mut archive = ZipArchive::new(zip_file)?;

    archive
        .file_names()
        .map(|s| s.to_string())
        .filter(|s| s != "umo.exe")
        .collect::<Vec<String>>()
        .iter()
        .for_each(|name| {
            if !name.contains("umo.exe") {
                extract_single_archive_entry(name, &mut archive)
            }
        });

    remove_file(zip_path).expect("Failed to remove temporary zip file after update!");
    Ok(())
}

/// Extracts a single entry from the umo-win.zip archive,
/// Creating directories when necessary and correcting directory separators to match the host operating system
/// Since what we're extracting is essentially the *entire* application, any failure here will
/// cause umo to delete the zip, the destination directory, and terminate, thus forcing an update.
fn extract_single_archive_entry(file_name: &String, archive: &mut ZipArchive<File>) {
    let file = archive.by_name(&file_name);
    match file {
        Ok(file_data) => {
            if let Some(internal_name) = &file_data.enclosed_name() {
                let path = get_archive_file_path_for_platform(&internal_name);

                create_path_parent(&path);

                match file_data {
                    mut file_data if file_data.is_file() => {
                        write_archive_file_to_disk(&path, &mut file_data);
                    }
                    file_data if file_data.is_dir() => {
                        if let Err(error) = create_dir_all(&path) {
                            abandon_extract(&format!(
                                "Failed to create archive directory {:?} : {}!",
                                &path, error
                            ));
                        };
                    }
                    _ => {
                        abandon_extract(&format!(
                            "All archive contents should be file or directory! {} is neither!",
                            file_name
                        ));
                    }
                }
            } else {
                abandon_extract(&format!(
                    "Unable to read enclosed name for {} in zip file! Bailing on extraction!",
                    file_name
                ));
            }
        }
        Err(error) => {
            abandon_extract(&format!(
                "Failed to get file {} in the update archive. Abandoning update due to {}!",
                file_name, error
            ));
        }
    }
}

/// Corrects path names inside of the umo-win archive, when necessary
fn get_archive_file_path_for_platform(archive_file_path: &PathBuf) -> PathBuf {
    PathBuf::from(
        get_python_path()
            .parent()
            .expect("Failed to get archive parent directory!")
            .join(
                archive_file_path
                    .to_string_lossy()
                    .replace('\\', &MAIN_SEPARATOR.to_string()),
            ),
    )
}

/// Gets a request::Response from a given url, retrying up to MAX_DOWNLOAD_ATTEMPTS times
fn get_download_response(client: &Client, url: &str) -> Result<Response, Error> {
    for attempt in 1..=MAX_DOWNLOAD_ATTEMPTS {
        umo_debug(&format!("Sending data to {}", url));
        match client.get(url).send() {
            Ok(resp) => return Ok(resp),
            Err(e) => {
                eprintln!("Download attempt {} failed: {}", attempt, e);
                if attempt == MAX_DOWNLOAD_ATTEMPTS {
                    return Err(addr_not_available_error(&format!(
                        "Download failed after {} attempts!",
                        MAX_DOWNLOAD_ATTEMPTS
                    ))
                    .unwrap_err());
                }
                continue;
            }
        };
    }
    panic!("Unreachable code!");
}

/// String formatter to get the download url for a given package file id in GitLab
fn get_download_url_for_file_id(manifest: JSONValue) -> Result<String, Error> {
    match manifest["id"].as_i64() {
        Some(id) => Ok(UMO_PACKAGE_FILES_URL.replace("{}", &id.to_string())),
        None => {
            Err(invalid_data_error("Failed to read file ID in GitLab API response!").unwrap_err())
        }
    }
}

/// Decide whether or not to force an update
/// due to being in an unbootable state
fn get_force_update() -> bool {
    let umo_root_dir = get_python_path();

    umo_debug(&umo_root_dir);
    if metadata(&umo_root_dir).is_err() {
        eprintln!("Unable to locate the umo root directory! umo will reinstall itself before initializing!");
        return true;
    }

    for (file, error_msg) in REQUIRED_FILES {
        if metadata(umo_root_dir.join(file)).is_err() {
            eprintln!("{}! umo will update before initializing!", error_msg);
            return true;
        }
    }

    false
}

/// Queries the gitlab api for the latest umo package and return it as a vector of JSON values
fn get_latest_package_as_json(client: &Client) -> Result<JSONValue, Error> {
    let mut json_value = None;

    for attempt in 1..=MAX_DOWNLOAD_ATTEMPTS {
        if let Ok(response) = parse_text_response(client, UMO_LATEST_PACKAGE_URL) {
            json_value = Some(response);
            break;
        } else if attempt == MAX_DOWNLOAD_ATTEMPTS {
            invalid_data_error("Too many download attempts for latest package json, bailing!")?
        }
    }

    Ok(json_from_str::<Vec<JSONValue>>(&json_value.unwrap())
        .map_err(|e| {
            invalid_data_error(&format!(
                "Unable to parse packages from gitlab API to json: {:#?} !",
                e
            ))
            .unwrap_err()
        })?
        .remove(0))
}

/// Pulls the latest package file manifest and minisig
fn get_latest_package_files(client: &Client, files_url: &str) -> Option<(JSONValue, JSONValue)> {
    let files_data = parse_text_response(&client, &files_url).ok()?;
    let mut files_json = json_from_str::<Vec<JSONValue>>(&files_data)
        .ok()?
        .into_iter();

    let package_file = files_json.find(|f| f["file_name"].as_str() == Some(&DOWNLOAD_ZIP))?;
    let minisig_file = files_json
        .find(|f| f["file_name"].as_str() == Some(&format!("{}{}", DOWNLOAD_ZIP, ".minisig")))?;

    Some((package_file.clone(), minisig_file.clone()))
}

/// Gets the path to the python binary
fn get_python_bin() -> PathBuf {
    get_python_path().join("python.exe")
}

/// Gets the local python path as a pathbuf
fn get_python_path() -> PathBuf {
    current_exe()
        .expect("Failed to get current executable!")
        .parent()
        .expect("Unable to get current executable parent directory!")
        .to_path_buf()
        .join("umo")
}

/// Returns the public key constant
fn get_pub_key() -> PublicKey {
    PublicKey::decode(&PUB_KEY).expect("Failed to get public minisign key from embedded string!")
}

/// Parses a string from a reqwest response and maps failures to std::io:error
fn parse_text_response(client: &Client, url: &str) -> Result<String, Error> {
    let response = get_download_response(client, url);
    umo_debug(&response);
    response
        .map_err(|e| Error::new(ErrorKind::InvalidData, format!("{:?}", e)))?
        .text()
        .map_err(|e| Error::new(ErrorKind::InvalidData, format!("{:?}", e)))
}

/// Parses bytes from a reqwest response and maps failures to std::io:error
fn parse_byte_response(client: &Client, url: &str) -> Result<Bytes, Error> {
    let response = get_download_response(client, url);
    umo_debug(&response);
    response
        .map_err(|e| Error::new(ErrorKind::InvalidData, format!("{:?}", e)))?
        .bytes()
        .map_err(|e| Error::new(ErrorKind::InvalidData, format!("{:?}", e)))
}

/// Given a reqwest::Blocking::Client and a download url,
/// Gets the response and saves it to a zip file in the current working directory
fn save_response_to_zip(
    client: &Client,
    download_url: &String,
    minisig: Signature,
) -> Result<(), Error> {
    let mut response_bytes = None;
    for attempt in 1..=MAX_DOWNLOAD_ATTEMPTS {
        match parse_byte_response(&client, download_url) {
            Ok(bytes) => {
                response_bytes = Some(bytes);
                break;
            }
            Err(e) => {
                eprintln!("Bytes extraction attempt failed: {:?}", e);
                if attempt == MAX_DOWNLOAD_ATTEMPTS {
                    invalid_data_error(&format!(
                        "Download failed after {} attempts!",
                        MAX_DOWNLOAD_ATTEMPTS
                    ))?
                }
            }
        };
    }

    let bytes_array = &response_bytes.as_ref().unwrap()[..];

    if let Err(error) = get_pub_key().verify(bytes_array, &minisig, false) {
        return invalid_data_error(&format!(
            "Public key verification FAILED: {:?}! Please try to update again or redownload umo.exe!",
            error
        ));
    };

    let zip_path = get_python_path()
        .parent()
        .expect("Failed to get archive parent directory!")
        .join(THE_ZIP);

    if metadata(&zip_path).is_ok() {
        remove_file(&zip_path)?
    }

    let mut temp_file = File::create(&zip_path)?;

    copy(
        &mut response_bytes
            .expect("Response bytes already validated!")
            .as_ref(),
        &mut temp_file,
    )?;

    Ok(())
}

/// Write the bytes in file_data to the file referenced by path
/// Terminates the application on failure
fn write_archive_file_to_disk(path: &PathBuf, file_data: &mut ZipFile) {
    match File::create(&path) {
        Ok(mut out_file) => {
            if let Err(_) = copy(file_data, &mut out_file) {
                abandon_extract("Failed to write archive file data to disk!");
            }
        }
        Err(error) => abandon_extract(&format!("Failed to create archive file {}!", error)),
    }
}

/// Prints and value with the Debug trait
/// if the environment variable UMO_DEBUG is set
fn umo_debug<T: Debug>(value: &T) {
    if var("UMO_DEBUG").is_ok() {
        dbg!(value);
    }
}

// Error Handlers

/// Address not available error used when very early portions of version detection fail
fn addr_not_available_error(error_string: &str) -> Result<(), Error> {
    Err(Error::new(ErrorKind::AddrNotAvailable, error_string))
}

/// Invalid data error used for most failed reads
fn invalid_data_error(error_string: &str) -> Result<(), Error> {
    Err(Error::new(ErrorKind::InvalidData, error_string))
}

// Core functionality
/// Reads the umo version.txt file and determines if an update is necessary
/// Returns Ok if no update is required. Otherwise, returns ErrorKind::InvalidData
fn check_needs_update(latest_version: &str) -> Result<(), Error> {
    let version_path = get_python_path().join("version.txt");

    if metadata(&version_path).is_err() {
        invalid_data_error("Update required as the version file could not be found!")?
    }

    let mut version_file = File::open(&version_path)?;
    let mut buffer = Vec::new();

    version_file.read_to_end(&mut buffer)?;

    let current_version = match String::from_utf8(buffer.clone()) {
        Ok(string) => string,
        Err(_) => {
            let version = Vec::from_iter(buffer.iter());
            let version: Vec<u16> = version
                .chunks_exact(2)
                .into_iter()
                .map(|a| u16::from_ne_bytes([*a[0], *a[1]]))
                .collect();
            let version = version.as_slice();

            if let Ok(version_string) = String::from_utf16(version) {
                version_string
                    .strip_prefix("\u{feff}")
                    .unwrap_or("")
                    .to_string()
            } else {
                return invalid_data_error(
                    "Couldn't read version file as utf-8 or utf-16! Bailing on update!",
                );
            }
        }
    };

    let current_version: String = current_version
        .strip_prefix("Version ")
        .unwrap_or(&current_version)
        .trim_end()
        .to_string();

    umo_debug(&format!(
        "Current version is: {}, Latest is: {}",
        &current_version, latest_version
    ));

    if current_version != latest_version {
        invalid_data_error("Update required as umo is out of date!")?
    }

    Ok(())
}

/// Pulls latest umo version out of the gitlab api, after checking whether an update is necessary.
/// Also handles extraction and cleanup.
/// If extraction fails, umo terminates. Any other failure state is non-fatal.
fn update_from_gitlab_package(force: bool) -> Result<(), Error> {
    let client = ClientBuilder::new()
        .timeout(Duration::from_secs(90))
        .connect_timeout(Duration::from_millis(500))
        .build()
        .expect("Failed to create reqwest client!");

    let latest = get_latest_package_as_json(&client)?;
    let version_string = latest["version"]
        .as_str()
        .expect("Failed to read version string from package JSON!");

    let needs_update = check_needs_update(version_string);

    if !force && needs_update.is_ok() {
        return Ok(());
    }

    umo_debug(&needs_update);

    umo_debug(&latest);

    let files_url = format!(
        "https://gitlab.com/api/v4/projects/modding-openmw%2Fumo/packages/{}/package_files",
        latest["id"].as_i64().unwrap()
    );

    let (pkg_file_manifest, minisig_file_manifest) =
        match get_latest_package_files(&client, &files_url) {
            Some((pkg_file, sig_file)) => (pkg_file, sig_file),
            None => return invalid_data_error("Failed to acquire update file manifests!"),
        };

    umo_debug(&minisig_file_manifest);
    umo_debug(&pkg_file_manifest);

    let signature_url = get_download_url_for_file_id(minisig_file_manifest)?;

    let signature_text = parse_text_response(&client, &signature_url)?;

    let signature = Signature::decode(&signature_text)
        .map_err(|e| invalid_data_error(&format!("{:#?}", e)).unwrap_err())?;

    let package_url = get_download_url_for_file_id(pkg_file_manifest)?;

    save_response_to_zip(&client, &package_url, signature)?;

    let _ = remove_dir_all(get_python_path());

    extract_archive()?;

    Ok(())
}

/// Check for updates and install them if necessary
/// Then call python.exe to run umo.py
fn main() -> ExitCode {
    let force_update = get_force_update();

    if var("UMO_NO_UPDATE").is_err() {
        if let Err(error) = update_from_gitlab_package(force_update) {
            if force_update {
                panic!("{}", &format!("Umo required an update to launch, but failed to get one. Please try running umo again or report this to umo developers: {}!", error))
            } else {
                eprintln!("Update attempt failed: {}", error);
            }
        };
    }

    let mut args: Vec<String> = vec![get_python_path()
        .join("umo.py")
        .to_string_lossy()
        .to_string()];

    args.extend(startup_args().skip(1).collect::<Vec<String>>());

    let result = Command::new(get_python_bin())
        .args(args)
        .status()
        .expect("Unable to launch python interpreter!");

    ExitCode::from(result.code().unwrap_or(0) as u8)
}
