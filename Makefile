ARCHITECTURE := arm64 # TODO universal2
DEST_BINDIR := /usr/local/bin
PYTHON := python3
APPIMAGETOOL := appimagetool
NUITKA_STANDALONE := 0

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	PLATFORM := linux
  ifeq ($(NUITKA_STANDALONE), 0)
	NUITKA_FLAGS := --onefile --onefile-no-compression --include-package=dbus_fast
  else
        NUITKA_FLAGS := --standalone --include-package=dbus_fast
  endif

endif
ifeq ($(UNAME_S),Darwin)
	PLATFORM := darwin
	NUITKA_FLAGS := --include-package=rubicon
endif

.PHONY: dist/umo.app/Contents/Info.plist
.PHONY: help
.PHONY: deps
.PHONY: install
.PHONY: all
.PHONY: format

ifeq ($(UNAME_S),Linux)
all: dist/umo
else
all: codesign
endif

format:
	@find src -name '*.py' -exec isort {} \;
	@find src -name '*.py' -exec black --unstable {} \;

clean:
	rm -Rf dist
	rm -Rf appimage/{AppDir,appimage-build,umo}

help:
	@echo Run "make install" to install the xdg-handler

install: $(DEST_BINDIR)/umo

uninstall:
	rm -rf $(DEST_BINDIR)/umo

.venv/bin/pip:
	$(PYTHON) -m venv .venv

.venv/lib: pyproject.toml .venv/bin/pip
	.venv/bin/pip install -e .
	@touch .venv/lib

$(DEST_BINDIR)/umo: dist/umo
	cp $$(readlink -f dist/umo) $(DEST_BINDIR)/

.venv/bin/nuitka: .venv/bin/pip
	.venv/bin/pip install -e .[nuitka]

ifeq ($(UNAME_S),Darwin)
.venv/lib/.pyobjc: .venv/lib
	.venv/bin/pip install pyobjc
	@touch .venv/lib/.pyobjc

dist/umo: dist/umo.app/Contents/Info.plist
	cp macos/umo dist/umo

dist/umo.app/Contents/Info.plist: dist/umo.app
	cp macos/umo.plist dist/umo.app/Contents/Info.plist
	rm -Rf dist/umo.dist # why ever this isn't cleaned up

.PHONY: codesign
codesign: dist/umo
	codesign --force --deep -s - dist/umo.app

dist/umo.app: .venv/bin/nuitka src/*.py .venv/lib/.pyobjc
else
dist/umo: .venv/bin/nuitka src/*.py .venv/lib
endif
	.venv/bin/python -m nuitka src/umo.py $(NUITKA_FLAGS)       \
	--deployment                                                \
	--assume-yes-for-downloads                                  \
	--include-module=desktop_notifier.resources                 \
	--include-package-data=desktop_notifier                     \
	--output-filename=umo                                       \
	--output-dir=dist                                           \
	--remove-output                                             \
	--macos-create-app-bundle                                   \
	--macos-target-arch=$(ARCHITECTURE)                         \
	--macos-app-icon=icons/KeenderL.png                         \
	--macos-signed-app-name=com.modding-openmw.umo              \
	--macos-app-name=com.modding-openmw.umo                     \
	--macos-app-version=0.8.18                                   \
	--linux-icon=icons/KeenderM.png                             \
	--product-name=umo                                          \
	--file-version=0.8.18

web-all: web-clean web-build local-web

web-debug-all: web-clean web-debug local-web

web-clean:
	cd web && rm -rf build site/*.md sha256sums soupault-*-linux-x86_64.tar.gz ../README.html web/site/index.html

web-build:
	cd web && ./build.sh

web-debug:
	cd web && ./build.sh --debug

web-verbose:
	cd web && ./build.sh --verbose

local-web:
	cd web && python3 -m http.server -d build

.venv/bin/appimage-builder: .venv/bin/pip
	.venv/bin/pip install -e .[appimage]

appimage/umo: src/*.py pyproject.toml appimage/AppImageBuilder.yml .venv/bin/appimage-builder
	cd appimage && ../.venv/bin/appimage-builder --skip-appimage
	rm -Rf appimage/AppDir/usr/share/{locale,doc,terminfo,man,aclocal,audit-rules,bash-completion,common-lisp,et,factory,fish,gtk-doc,info,keyutils,libalpm,libgpg-error,licenses,p11-kit,readline,ss,tabset,zsh}
	rm -Rf appimage/AppDir/usr/include
	rm -Rf appimage/AppDir/usr/lib/{pkgconfig,p11-kit,pkcs11,libsystemd.so.*,cmake}
	find appimage/AppDir/usr/bin ! -name 'python*' ! -type d -exec rm -f "{}" \;
	rm -Rf appimage/AppDir/runtime/compat/usr/share
	rm -Rf appimage/AppDir/runtime/compat/usr/include
	find appimage/AppDir/runtime/compat/usr/bin ! -name 'python*' ! -type d -exec rm -f "{}" \;
	#find appimage/AppDir/usr/lib/python3.13/site-packages/dbus_fast -name '*.so' -exec rm -f "{}" \;
	#find appimage/AppDir/usr/lib/python3.13/site-packages/dbus_fast -name '*.pxd' -exec rm -f "{}" \;
	rm -Rf appimage/AppDir/usr/lib/python3.13/config-3.13-x86_64-linux-gnu
	rm -Rf appimage/AppDir/usr/lib/python3.13/ensurepip
	#python -m compileall appimage/AppDir/usr/lib/python3.13/
	#python -m compileall appimage/AppDir/usr/src/
	# rm -Rf appimage/AppDir/usr/src/libsqlite*
	# rm -Rf appimage/AppDir/usr/src/libcrypto*
	# rm -Rf appimage/AppDir/usr/src/libssl*
	# rm -Rf appimage/AppDir/usr/src/libexpat*
	# rm -Rf appimage/AppDir/usr/src/libffi*
	# rm -Rf appimage/AppDir/usr/src/libreadline*
	# rm -Rf appimage/AppDir/usr/src/libncursesw*
	# rm -Rf appimage/AppDir/usr/src/libmpdec*
	# rm -Rf appimage/AppDir/usr/src/libuuid*
	cd appimage && $(APPIMAGETOOL) AppDir umo --mksquashfs-opt -no-compression # --comp zstd --comp zstd --mksquashfs-opt -Xcompression-level --mksquashfs-opt 1
