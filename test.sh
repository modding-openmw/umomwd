#!/bin/bash

if [ $(uname -s) == "Darwin" ]
then
   config_dir="$HOME/Library/Application Support/umomwd"
   cache_dir="$HOME/Library/Caches/umomwd"
else
   config_dir="$HOME/.config/umomwd"
   cache_dir="$HOME/.cache/umomwd"
fi

echo $config_dir
echo $cache_dir

function umo_test {
    testname=$1
    shift
    eval "$@" > ./testoutput 2>&1
    if [ ! $? == 0 ]
    then
        echo "NOT OK: $testname"
        cat ./testoutput
        exit 1
    else
        echo "OK: $testname"
    fi
}

mkdir -p "$config_dir"
mkdir -p "$cache_dir"
if [ ! -f "${config_dir}/config.json" ]
then
    cp dummy.cnf "${config_dir}/config.json"
fi
ls -Al "${config_dir}"
ls -Al "${cache_dir}"

# trick umo into thinking we have everything we need
touch tes3cmd
chmod +x tes3cmd
mkdir -p "Data Files"

umo_test cache-sync .venv/bin/python src/umo.py cache sync expanded-vanilla -S MOMWGameplay
umo_test install .venv/bin/python src/umo.py install --download-only --no-gui expanded-vanilla -S MOMWGameplay
umo_test install-check-archive "ls ${cache_dir}/archive-cache/MOMWGameplay/ | grep .archive"
umo_test install .venv/bin/python src/umo.py install --no-gui expanded-vanilla -S MOMWGameplay
umo_test install-check-extracted "ls OpenMWMods/expanded-vanilla/Gameplay/MOMWGameplay/ | grep momw-gameplay-ui.omwscripts"
umo_test cache-query ".venv/bin/python src/umo.py cache query momw expanded-vanilla.mods_by_name.MOMWGameplay | grep MOMWGameplay"
umo_test cache-dump ".venv/bin/python src/umo.py cache dump expanded-vanilla | grep MOMWGameplay"
.venv/bin/python src/umo.py cache dump expanded-vanilla > expanded-vanilla-patched
.venv/bin/python src/umo.py cache add expanded-vanilla-patched
rm expanded-vanilla-patched
umo_test cache-add ".venv/bin/python src/umo.py cache query momw expanded-vanilla-patched.mods_by_name.ProjectAtlas | grep ProjectAtlas"
