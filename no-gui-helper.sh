#!/bin/bash

f=".umo-url"
e=$1

while inotifywait -e close_write $f 2> /dev/null
do
    url=$(cat $f)
    $e $url
done
