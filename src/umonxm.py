# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# this file is a partly port of pynxm to pycurl
# (https://github.com/dh-nunes/pynxm)

import json
from email.parser import BytesParser
from io import BytesIO

import pycurl

import dl

BASE_URL = "https://api.nexusmods.com/v1/"


class LimitReachedError(Exception):
    """
    Exception raised when the request rate limit has been reached.
    """

    pass


class RequestError(Exception):
    """
    Exception raised when a request returns an error code.
    """

    pass


class Nxm:
    def __init__(self, api_key):
        self.api_key = api_key
        self.last_headers = None

    def _check_status_code(status_code, body):
        if status_code not in (200, 201):
            if status_code == 429:
                raise LimitReachedError("You have reached your NEXUS request limit.")
            else:
                raise RequestError(f"Status Code {status_code} - {body}")

    def _make_request(self, url):
        url = BASE_URL + url
        resp_headers = BytesIO()
        body = dl.curl_get_body(
            url,
            [f"apikey: {self.api_key}", f"content-type: application/json"],
            resp_headers,
            Nxm._check_status_code,
        )
        _, raw_header = resp_headers.getvalue().split(b"\r\n", 1)
        self.last_headers = dict(BytesParser().parsebytes(raw_header))
        return json.loads(body)

    def get_limits(self):
        if not self.last_headers:
            self.user_details()

        return {
            "hour_limit": self.last_headers["x-rl-hourly-limit"],
            "hour_remaining": self.last_headers["x-rl-hourly-remaining"],
            "hour_reset": self.last_headers["x-rl-hourly-reset"],
            "day_limit": self.last_headers["x-rl-daily-limit"],
            "day_remaining": self.last_headers["x-rl-daily-remaining"],
            "day_reset": self.last_headers["x-rl-daily-reset"],
        }

    def user_details(self):
        return self._make_request("users/validate.json")

    def md5_search(self, game, md5_hash):
        return self._make_request(f"games/{game}/mods/md5_search/{md5_hash}.json")

    def mod_details(self, game, mod_id):
        return self._make_request(f"games/{game}/mods/{mod_id}.json")

    def mod_file_details(self, game, mod_id, file_id):
        return self._make_request(f"games/{game}/mods/{mod_id}/files/{file_id}.json")

    def mod_file_list(self, game, mod_id, category=None):
        url = f"games/{game}/mods/{mod_id}/files.json"
        if isinstance(category, str):
            url = url + f"?category={category}"
        elif isinstance(category, list):
            category = ",".join(category)
            url = url + f"?category={category}"

        return self._make_request(url)

    def mod_file_download_link(self, game, mod_id, file_id, key=None, expires=None):
        url = f"games/{game}/mods/{mod_id}/files/{file_id}/download_link.json"
        if key and expires:
            url = url + f"?key={key}&expires={expires}"

        return self._make_request(url)
