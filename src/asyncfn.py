# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
from multiprocessing.pool import ThreadPool

import tqdm

import helper
import log
from config import Config


class FnLogWrapper:
    def __init__(self, fn, mod=None, archive=None):
        self.fn = fn
        self.mod = mod
        self.archive = archive


def run_async(
    fns, progress=False, total=10, desc=None, colour="cyan", desc_log=None, threads=None
):
    log.json_log_state(desc_log or desc, "start", None, 0, total)
    with ThreadPool(threads or Config.MAX_THREADS) as pool:
        res = []
        for fn in fns:
            res.append((pool.apply_async(fn.fn), fn))

        pb = tqdm.tqdm(
            disable=not progress or Config.JSON_OUTPUT,
            total=total,
            desc=desc,
            colour=colour,
            bar_format="{l_bar}{bar}| {n_fmt}/{total_fmt}",
        )

        done = []
        curr = 0
        while len(done) < total:
            time.sleep(0.1)
            for x in res:
                r = x[0]
                wrapper = x[1]
                if r not in done and r.ready():
                    r.get()
                    done.append(r)
                    curr += 1
                    pb.update(1)
                    pb.refresh(nolock=True)
                    log.json_log_state(
                        desc_log or desc, "done", None, curr, total, wrapper.mod
                    )
        log.json_log_state(desc_log or desc, "complete")


def slurp_async(
    fn, l, progress=False, total=10, initial=0, desc=None, colour="cyan", threads=None
):
    tres = []
    done = []
    current = 0
    pb = tqdm.tqdm(
        disable=not progress or Config.JSON_OUTPUT,
        total=total,
        desc=desc,
        colour=colour,
        bar_format="{l_bar}{bar}| {n_fmt}/{total_fmt}",
        initial=initial,
    )
    pb.refresh(nolock=True)

    with ThreadPool(threads or Config.MAX_THREADS) as pool:
        take = helper.take_n_gen(l, 1)
        while True:
            if current <= Config.MAX_THREADS:
                took = next(take, -1)
                if took == -1:
                    while len(done) < len(tres):
                        time.sleep(0.1)
                        for r in tres:
                            if r not in done and r.ready():
                                r.get()
                                pb.update(1)
                                pb.refresh(nolock=True)
                                done.append(r)
                    break

                first = took[0]
                fns = fn(first)
                for f in fns:
                    tres.append(pool.apply_async(f))
                    current += 1
                    for r in tres:
                        if r not in done and r.ready():
                            r.get()
                            done.append(r)
                            pb.update(1)
                            pb.refresh(nolock=True)
                            current -= 1

            else:
                time.sleep(0.1)
                for r in tres:
                    if r not in done and r.ready():
                        r.get()
                        done.append(r)
                        pb.update(1)
                        pb.refresh(nolock=True)
                        current -= 1
