# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import os
import platform
import re
import shutil
import subprocess
import sys
import time
import traceback
from pathlib import Path

import curldl.util

import cache
import helper
import mega
import requests
import umonxm
import wayback
from config import Config
from dl import WTF, curl_get_content_length, download, extract
from helper import download_paths, where
from log import get_logger, json_log_state
from momw_types import CopyAction, DownloadInfo, ModDesc, RemoveAction, rmtree_onerror

logger = get_logger()


def cache_content_length(mod, info, url):
    clen = None
    if url.startswith("https://mega.nz/"):
        clen = mega.get_size(url)
    else:
        clen = curl_get_content_length(url)
    if not clen > 0:
        clen = None
    cache.cache_synced(
        mod, info, "generic_data", cache.generic_data(None, size=clen, url=url)
    )


def check_size(path, generic_data):
    fsize = os.path.getsize(path)
    res = False
    hash_info = generic_data.get("hash", None)
    if generic_data.get("size_in_bytes"):
        res = fsize == generic_data["size_in_bytes"]
    elif generic_data.get("size_kb"):
        res = round(fsize / 1024) == generic_data["size_kb"]
    if (
        hash_info
        and hash_info["type"]
        and hash_info["value"]
        and hash_info["value"] not in WTF
    ):
        hash_info = generic_data.get("hash")
        try:
            curldl.util.Cryptography.verify_digest(
                path, hash_info["type"], hash_info["value"]
            )
        except:
            logger.debug(f"hash doesn't match {path}: {hash_info}")
            return False
    return res


def mod_archive_exists(mod: ModDesc, data: DownloadInfo, shallow=False):
    synced = cache.get_synced(mod, data)
    installed = cache.get_installed(mod, data)
    version = synced.get("generic_data", {}).get("version")
    _, archive = download_paths(mod.name, data.file_name, version)
    _, archive_sans_version = download_paths(mod.name, data.file_name)

    if Path(archive_sans_version).exists() and archive != archive_sans_version:
        if check_size(archive_sans_version, synced.get("generic_data", {})):
            if Config.VERBOSE:
                logger.warning(f"renaming old archive: {archive_sans_version}")
            os.rename(archive_sans_version, archive)
        else:
            if Config.VERBOSE:
                logger.warning(f"discarding old archive: {archive_sans_version}")
            os.unlink(archive_sans_version)

    if Path(archive).exists() and not shallow:
        if check_size(archive, synced.get("generic_data", {})):
            if not installed.get("downloaded"):
                if Config.VERBOSE:
                    logger.warning(f"adding lose archive to cache: {archive}")
            cache.cache_installed(mod, data, "downloaded", True)
        else:
            if not installed.get("downloaded"):
                if Config.VERBOSE:
                    logger.warning(f"discarding lose archive: {archive}")
            else:
                if Config.VERBOSE:
                    logger.warning(f"updating old archive: {archive}")
            cache.cache_installed(mod, data, "downloaded", False)
            os.unlink(archive)

    if cache.get_installed(mod, data).get("downloaded") and Path(archive).exists():
        return True

    return False


def mod_already_exists(mod: ModDesc, data: DownloadInfo):
    synced = cache.get_synced(mod, data)
    installed = cache.get_installed(mod, data)
    if not Path(where(mod)).exists():
        return False

    # we may have altered direct_download urls... so can't check those
    # here
    imod_data = installed.get("mod_data")
    cmod_data = json.loads(cache.to_json(data, exclude={"direct_download"}))
    if imod_data:
        imod_data["direct_download"] = ""
        if not cmod_data.get("nexus_file_id", None):
            imod_data["nexus_file_id"] = ""
            cmod_data["nexus_file_id"] = ""
    cmod_data["direct_download"] = ""

    if imod_data:
        # reset action vars - those are also cached ...
        for action in imod_data["actions"]:
            action["mw_dir"] = None
            action["tes3cmd"] = None

    # did the momw data change?
    if imod_data != cmod_data:
        return False

    if installed.get("generic_data") and synced.get("generic_data"):
        installed_size = installed.get("generic_data").get("size_in_bytes")
        installed_size_kb = installed.get("generic_data").get("size_kb")
        installed_version = installed.get("generic_data").get("version")
        synced_size = synced.get("generic_data").get("size_in_bytes")
        synced_size_kb = synced.get("generic_data").get("size_kb")
        synced_version = synced.get("generic_data").get("version")

        if synced_version != installed_version:
            return False

        if installed.get("actions_done"):
            if installed_size is not None:
                if installed_size == synced_size:
                    return True
            elif installed_size_kb is not None:
                if installed_size_kb == synced_size_kb:
                    return True
            elif (
                installed_size is None
                and installed_size_kb is None
                and synced_size_kb is None
                and synced_size is None
            ):
                return True

    return False


# TODO refactor me
def check_mod_exists(mod: ModDesc):
    for data in mod.download_info:
        if not mod_already_exists(mod, data):
            cache.cache_installed(mod, data, "extracted", False)
            cache.cache_installed(mod, data, "actions_done", False)
            return False

    return True


def check_mod_archive_exists(mod: ModDesc):
    for data in mod.download_info:
        if not mod_archive_exists(mod, data):
            cache.cache_installed(mod, data, "downloaded", False)
            return False

    return True


def check_mod_extracted(mod: ModDesc, mods):
    res = True
    for data in mod.download_info:
        if not mod_archive_exists(mod, data, True):
            return True  # escape for mods that have some archives
            # not downloaded

        if not mod_already_exists(mod, data):
            installed = cache.get_installed(mod, data)

            if not installed.get("extracted"):
                res = False

        # check conflicts
        for other in mods:
            for c in mod.conflicts:
                if other.dir == c:
                    for info in other.download_info:
                        # got one and this mod needs an update so let's mark the other one also to non extracted
                        if not res:
                            cache.cache_installed(other, info, "extracted", False)
                            cache.cache_installed(other, info, "actions_done", False)

    if not res:
        cache.cache_installed(mod, data, "extracted", False)
        cache.cache_installed(mod, data, "actions_done", False)

    logger.debug(f"f{mod.dir} extracted: {res}")
    return res


def check_mod_actions_done(mod: ModDesc):
    res = True
    for data in mod.download_info:
        installed = cache.get_installed(mod, data)

        if not mod_archive_exists(mod, data, True):
            return True  # escape for mods that have some archives
            # not downloaded

        if not installed.get("extracted"):
            return True

        if not installed.get("actions_done"):
            res = False

    return res


def handle_actions(mod: ModDesc, data: DownloadInfo, basedir):
    for action in data.actions:
        action.mw_dir = Config.MORROWIND_DIR
        action.tes3cmd = Config.TES3CMD
        json_log_state(
            "run_actions",
            "action_init",
            json.loads(action.json()),
            mod=mod.name,
            archive=data.file_name,
        )
        action.handle(basedir)
        json_log_state(
            "run_actions",
            "action_done",
            json.loads(action.json()),
            mod=mod.name,
            archive=data.file_name,
        )


def read_socket(test):
    Config.SOCKET_SERVER.listen(1)
    if Config.VERBOSE:
        logger.info("Waiting for socket input...")
    connection, _ = Config.SOCKET_SERVER.accept()
    s = ""
    try:
        while True:
            data = connection.recv(1024)
            if not data:
                break
            else:
                s = s + data.decode()
            if test(s):
                connection.send(b"OK")
            else:
                connection.send(b"NOPE")
    finally:
        connection.close()

    return s


class BaseHandler:
    def can(self, mod: ModDesc):
        pass

    def _sync(self, mod: ModDesc):
        pass

    def sync(self, mod: ModDesc):
        json_log_state("syncing_mod_data", "init", mod=mod.name)
        self._sync(mod)
        json_log_state("syncing_mod_data", "done", mod=mod.name)

    def prepare(self, mod: ModDesc):
        pass

    def _handle(self, mod: ModDesc, genfn, clean=False, lazy=False):
        later = []
        forceall = False

        # and if not every archive exists
        for data in mod.download_info:
            if not mod_already_exists(mod, data):
                forceall = True
                break

        if not forceall:
            return []

        # first we save all our functions to get all archives
        for data in mod.download_info:
            fn = genfn(mod, data)
            later.append(fn)

        # we download all of them - just to be sure to not mess up the
        # directory on updates
        # keep in mind that our archives are cached so in best case we only
        # extract again

        # reset mod dir - we want a clean directory before extracting
        if clean:
            mod_dir = where(mod)
            if Path(mod_dir).exists():
                shutil.rmtree(mod_dir, onerror=rmtree_onerror)

        if lazy:
            return later
        else:
            for fn in later:
                fn()

    def download(self, mod: ModDesc):
        def gen(_mod, _data):
            def d():
                try:
                    nonlocal _mod
                    nonlocal _data
                    base = f"{Config.BASEPATH}/{_mod.category}/"
                    download(_mod, _data)
                except Exception as ex:
                    logger.error(f"{_mod.name} error occured - skipping: {ex}")
                    json_log_state("downloading", "error", f"{ex}", mod=_mod.name)
                    if Config.VERBOSE:
                        traceback.print_exc()

            return d

        l = self._handle(mod, gen, False, True)

        for i in l:
            yield i

    def extract(self, mod: ModDesc):
        def gen(_mod, _data):
            def d():
                extract(_mod, _data)

            return d

        json_log_state("extracting", "init", mod=mod.name)
        self._handle(mod, gen, True, False)
        json_log_state("extracting", "done", mod=mod.name)

    def run_actions(self, mod: ModDesc):
        def gen(_mod, _data):
            def d():
                nonlocal _mod
                nonlocal _data

                c = cache.get_installed(_mod, _data)
                if not c:
                    c = {}

                if not c.get("extracted"):
                    return

                base = f"{Config.BASEPATH}/{_mod.category}/"
                try:
                    json_log_state(
                        "run_actions",
                        "archive_init",
                        None,
                        mod=_mod.name,
                        archive=_data.file_name,
                    )
                    handle_actions(_mod, _data, base)
                    # we skip the url - this is in most cases unique
                    # and expires and as we use mod_data to check if
                    # we need to update this comparing it to a new
                    # one, we'll ignore it
                    _data.direct_download = ""
                    cache.cache_installed(_mod, _data, "mod_data", _data)
                    cache.cache_installed(_mod, _data, "actions_done", True)
                    json_log_state(
                        "run_actions",
                        "archive_done",
                        None,
                        mod=_mod.name,
                        archive=_data.file_name,
                    )
                except Exception as e:
                    json_log_state(
                        "run_actions",
                        "error",
                        f"{e}",
                        mod=_mod.name,
                        archive=_data.file_name,
                    )
                    shutil.rmtree(where(_mod), onerror=rmtree_onerror)
                    raise e

            return d

        self._handle(mod, gen)


class DirectHandler(BaseHandler):
    def can(self, mod: ModDesc):
        if mod.download_info[0].direct_download:
            mod.handler = "direct"
            return True

        if mod.dl_url.startswith("/"):
            mod.set_single_dl_link("https://modding-openmw.com" + mod.dl_url)
            mod.handler = "direct"
            return True

        urlsp = mod.url.split("?")[0]
        if urlsp.endswith(".zip") or urlsp.endswith(".7z") or urlsp.endswith(".rar"):
            mod.set_single_dl_link(mod.url)
            mod.handler = "direct"
            return True

        return False

    def _sync(self, mod: ModDesc):
        for info in mod.download_info:
            json_log_state(
                "syncing_mod_data", "archive_init", mod=mod.name, archive=info.file_name
            )
            cache_content_length(mod, info, info.direct_download)
            json_log_state(
                "syncing_mod_data", "archive_done", mod=mod.name, archive=info.file_name
            )

    def prepare(self, mod: ModDesc):
        return mod


class GitSinkHandler(BaseHandler):
    dev = False

    def __init__(self):
        self.dev = Config.USE_DEV_MODS

    def _sync_dev(self, mod: ModDesc):
        pass

    def _sync_release(self, mod: ModDesc):
        pass

    def _sync(self, mod: ModDesc):
        if self.dev:
            self._sync_dev(mod)
        else:
            try:
                self._sync_release(mod)
            except:
                self._sync_dev(mod)


class GitlabHandler(GitSinkHandler):
    api_url = "https://gitlab.com/api/v4"
    dev = False

    def can(self, mod: ModDesc):
        if mod.url.startswith("https://gitlab.com/"):
            mod.handler = "raw-gitlab"
            return True
        return False

    def get_project_id(self, mod: ModDesc):
        splitted = list(filter(lambda x: x, mod.url.split("/")))
        return splitted[-2] + "%2F" + splitted[-1]

    def _sync_dev(self, mod: ModDesc):
        project_id = self.get_project_id(mod)
        last_commit = requests.get(
            f"{self.api_url}/projects/{project_id}/repository/commits"
        ).json()[0]["id"]
        url = f"{self.api_url}/projects/{project_id}/repository/archive.zip?sha={last_commit}"
        cache.cache_synced(
            mod,
            mod.download_info[0],
            "generic_data",
            cache.generic_data(last_commit, url=url),
        )

    def _sync_release(self, mod: ModDesc):
        # TODO package != release
        project_id = self.get_project_id(mod)
        packages_url = f"{self.api_url}/projects/{project_id}/packages"
        page = 1
        packages_data = requests.get(packages_url + f"?sort=desc&page={page}")
        package_data = packages_data.json()[0]

        package_files_url = (
            packages_url + "/" + str(package_data["id"]) + "/package_files"
        )
        package_file = requests.get(package_files_url).json()[0]  # always first?
        url = (
            package_file["pipelines"][0]["web_url"].split("pipelines")[0]
            + "package_files/"
            + str(package_file["id"])
            + "/download"
        )
        cache.cache_synced(mod, mod.download_info[0], "gitlab_data", package_file)
        cache.cache_synced(
            mod,
            mod.download_info[0],
            "generic_data",
            cache.generic_data(
                package_file["pipelines"][0]["ref"],
                size=package_file["size"],
                hash_digest=package_file["file_sha256"],
                hash_algo="sha256",
                url=url,
            ),
        )

    def prepare(self, mod: ModDesc):
        if mod.prepared:
            return mod

        mod.set_single_dl_link(
            cache.get_synced(mod, mod.download_info[0])["generic_data"]["url"]
        )

        version = cache.get_synced(mod, mod.download_info[0])["generic_data"]["version"]
        splitted = list(filter(lambda x: x, mod.url.split("/")))
        project_name = splitted[-1]
        project_id = splitted[-2] + "%2F" + project_name
        url = (
            f"{self.api_url}/projects/{project_id}/repository/archive.zip?sha={version}"
        )
        folder_name = f"{project_name}-{version}-{version}"
        if url == cache.get_synced(mod, mod.download_info[0])["generic_data"]["url"]:
            # inject action to move stuff out of the folder
            di = mod.download_info[0]
            remove = RemoveAction(
                action="remove", path=di.extract_to + "/" + folder_name
            )
            copy = CopyAction(
                action="copy",
                src=di.extract_to + "/" + folder_name,
                dst=di.extract_to,
                force=True,
            )
            di.actions = [copy, remove] + di.actions

        mod.prepared = True

        return mod


class MOMWGitlabHandler(GitlabHandler):

    def can(self, mod: ModDesc):
        if mod.url.startswith("https://modding-openmw.gitlab.io"):
            mod.handler = "gitlab"
            return True
        return False

    def _sync(self, mod: ModDesc):
        splitted = list(filter(lambda x: x, mod.url.split("/")))
        mod_name = ""
        multi_mod = False
        for idx, part in enumerate(splitted):
            if part == "modding-openmw.gitlab.io":
                mod_name = splitted[idx + 1]
                if len(splitted) > idx + 2:
                    multi_mod = splitted[idx + 2]
                break

        if not mod_name:
            raise Exception(f"couldn't determine mod name from: {mod.url}")

        if self.dev:  # for dev
            mod.url = f"https://gitlab.com/modding-openmw/{mod_name}"
            super()._sync(mod)
            return

        base_url = f"{self.api_url}/projects/modding-openmw%2F{mod_name}"
        packages_url = base_url + "/packages"
        page = 1
        packages_data = requests.get(packages_url + f"?sort=desc&page={page}")
        max_pages = int(packages_data.headers["x-total-pages"])
        package_data = packages_data.json()[0]
        if multi_mod:
            found = False
            while not found:
                for idx, d in enumerate(packages_data.json()):
                    if d["version"].startswith(multi_mod):
                        found = True
                        package_data = packages_data.json()[idx]
                        break
                if found:
                    break

                if page == max_pages:
                    raise Exception("couldn't find correct package in packages")
                page += 1
                packages_data = requests.get(packages_url + f"?sort=desc&page={page}")

        package_files_url = (
            packages_url + "/" + str(package_data["id"]) + "/package_files"
        )
        package_file = requests.get(package_files_url).json()[0]  # always first?
        url = (
            package_file["pipelines"][0]["web_url"].split("pipelines")[0]
            + "package_files/"
            + str(package_file["id"])
            + "/download"
        )
        cache.cache_synced(mod, mod.download_info[0], "gitlab_data", package_file)
        cache.cache_synced(
            mod,
            mod.download_info[0],
            "generic_data",
            cache.generic_data(
                package_file["pipelines"][0]["ref"],
                size=package_file["size"],
                hash_digest=package_file["file_sha256"],
                hash_algo="sha256",
                url=url,
            ),
        )


class GithubHandler(GitSinkHandler):
    def can(self, mod: ModDesc):
        if mod.url.startswith("https://github.com/"):
            mod.handler = "github"
            return True
        return False

    def _sync_dev(self, mod: ModDesc):
        splitted = list(filter(lambda x: x, mod.url.split("/")))
        repo = splitted[3]
        owner = splitted[2]
        last_commit = requests.get(
            f"https://api.github.com/repos/{owner}/{repo}/commits"
        ).json()[0]["sha"]
        url = f"https://github.com/{owner}/{repo}/archive/{last_commit}.zip"
        folder_name = f"{repo}-{last_commit}"
        cache.cache_synced(
            mod,
            mod.download_info[0],
            "generic_data",
            cache.generic_data(last_commit, url=url),
        )

    def _sync_release(self, mod: ModDesc):
        splitted = list(filter(lambda x: x, mod.url.split("/")))
        repo = splitted[3]
        owner = splitted[2]

        base_url = f"https://api.github.com/repos/{owner}/{repo}/releases"
        release = requests.get(base_url).json()[0]
        asset = release["assets"][0]
        size = asset["size"]
        url = asset["browser_download_url"]

        cache.cache_synced(mod, mod.download_info[0], "github_data", release)
        cache.cache_synced(
            mod,
            mod.download_info[0],
            "generic_data",
            cache.generic_data(
                release["name"],
                size=size,
                url=url,
            ),
        )

    def prepare(self, mod: ModDesc):
        if mod.prepared:
            return mod

        mod.set_single_dl_link(
            cache.get_synced(mod, mod.download_info[0])["generic_data"]["url"]
        )

        version = cache.get_synced(mod, mod.download_info[0])["generic_data"]["version"]
        splitted = list(filter(lambda x: x, mod.url.split("/")))
        repo = splitted[3]
        owner = splitted[2]
        folder_name = f"{repo}-{version}"
        url = f"https://github.com/{owner}/{repo}/archive/{version}.zip"
        if url == cache.get_synced(mod, mod.download_info[0])["generic_data"]["url"]:
            # inject action to move stuff out of the folder
            di = mod.download_info[0]
            remove = RemoveAction(
                action="remove", path=di.extract_to + "/" + folder_name
            )
            copy = CopyAction(
                action="copy",
                src=di.extract_to + "/" + folder_name,
                dst=di.extract_to,
                force=True,
            )
            di.actions = [copy, remove] + di.actions

        mod.prepared = True

        return mod


class BaturinHandler(BaseHandler):
    def can(self, mod: ModDesc):
        if mod.url.startswith("https://baturin.org"):
            mod.handler = "baturin"
            return True
        return False

    def _sync(self, mod: ModDesc):
        splitted = mod.url.split("#")
        url = splitted[0] + splitted[1].replace("-", "_") + ".zip"
        cache_content_length(mod, mod.download_info[0], url)

    def prepare(self, mod: ModDesc):
        if mod.prepared:
            return mod
        mod.set_single_dl_link(
            cache.get_synced(mod, mod.download_info[0])["generic_data"]["url"]
        )
        mod.prepared = True
        return mod


class GoogleDriveHandler(BaseHandler):
    def can(self, mod: ModDesc):
        if mod.url.startswith("https://drive.google"):
            mod.handler = "googledrive"
            return True
        return False

    def get_url(self, url):
        # see https://stackoverflow.com/questions/48133080/how-to-download-a-google-drive-url-via-curl-or-wget
        m = re.match(r".*file\/d\/(.*?)\/", url)
        if m:
            res = f"https://drive.usercontent.google.com/download?id={m.group(1)}&confirm=xxx"
            return res

        return None

    def _sync(self, mod: ModDesc):
        url = self.get_url(mod.url)
        if url:
            cache_content_length(mod, mod.download_info[0], url)
        else:
            logger.error("couldn't get google drive dl url: " + mod.name)
            logger.error("Skipping")

    def prepare(self, mod: ModDesc):
        if mod.prepared:
            return mod

        mod.set_single_dl_link(self.get_url(mod.url))
        mod.prepared = True

        return mod


class TRHandler(GoogleDriveHandler):
    def can(self, mod: ModDesc):
        if mod.url.startswith("http://www.tamriel-rebuilt.org"):
            mod.handler = "tr"
            return True
        return False

    def get_tr_url(self, mod: ModDesc):
        raw = requests.get(mod.url).content
        matches = re.findall(
            r"https:\/\/drive.google.com\/.*?usp=sharing\"", raw.decode()
        )
        index = 0
        if mod.url.endswith("resources"):
            index = 1

        return self.get_url(matches[index])

    def _sync(self, mod: ModDesc):
        url = self.get_tr_url(mod)
        if url:
            cache_content_length(mod, mod.download_info[0], url)
        else:
            logger.error("couldn't get google drive dl url: " + mod.name)
            logger.error("Skipping")

    def prepare(self, mod: ModDesc):
        if mod.prepared:
            return mod

        mod.set_single_dl_link(self.get_tr_url(mod))
        mod.prepared = True
        return mod


class NexusHandler(BaseHandler):
    nxm = None
    download_link = None

    def __init__(self):
        self.nxm = umonxm.Nxm(Config.NEXUS_API_KEY)

    def can(self, mod: ModDesc):
        url = mod.url
        m = re.match(r"https:\/\/www.nexusmods.com\/.*\/([0-9]+)", url)
        if m:
            g = m.group(1)
            mod.nexus_id = g
            mod.handler = "nexus"
            return True
        return False

    def _update(data, updates, all_files):
        for update in updates:
            if update["old_file_id"] == data.nexus_file_id:
                skip = False
                new_file_id = update["new_file_id"]
                for f in all_files:
                    if f["file_id"] == new_file_id and f["category_name"] == "UPDATE":
                        logger.debug(
                            f"skipping update of file_id {data.nexus_file_id} to"
                            f" {new_file_id} as this is a separate UPDATE file"
                        )
                        skip = True
                        break

                if skip:
                    continue

                logger.debug(f"updating file_id {data.nexus_file_id} to {new_file_id}")
                data.nexus_file_id = new_file_id
        return data

    def get_file_list(self, mod: ModDesc, mod_id, category):
        if cache.get_data(cache.NEXUS_CACHE_ID, f"$.{mod_id}"):
            logger.debug(f"cache hit: {mod_id}")
            return cache.get_data(cache.NEXUS_CACHE_ID, f"$.{mod_id}")

        try:
            r = self.nxm.mod_file_list("morrowind", mod_id, category)
            cache.set_data(cache.NEXUS_CACHE_ID, f"$.{mod_id}", r)
        except json.decoder.JSONDecodeError:
            print(f"""The Nexus API key you've entered appears to be invalid.
Please run this to re-enter it: {helper.get_real_argv0()} reconfig""")
            sys.exit(1)
        except umonxm.RequestError as ex:
            if re.search("Mod not available:", str(ex)) or re.search("403", str(ex)):
                # we got a hidden mod let's check the archives

                r = {"files": [], "file_updates": []}

                # either we have tracked the file id ourselves
                for i in mod.download_info:
                    if i.nexus_file_id:
                        fd = self.nxm.mod_file_details(
                            "morrowind", mod_id, i.nexus_file_id
                        )
                        r["files"].append(fd)

                # or we can try to check wayback machine
                if len(r.get("files", [])) == 0:
                    ids = wayback.get_archived_nexus_file_ids(mod_id)
                    if ids and len(ids) > 0:
                        for i in ids:
                            fd = self.nxm.mod_file_details("morrowind", mod_id, i)
                            r["files"].append(fd)
            else:
                raise ex

        return r

    # TODO refactor me...  this does too many things like syncing and
    # scraping and really getting download links
    def download_links(self, mod: ModDesc, sync=True, nosync=False):
        mod_id = mod.nexus_id
        d_info = mod.download_info

        guess = not d_info[0].file_name and not d_info[0].nexus_file_id

        dl_data = []
        if guess:
            logger.error("DATA not on modding-openmw :( - skipping")
        else:
            file_list_resp = self.get_file_list(
                mod,
                mod_id,
                ["main", "update", "optional", "miscellaneous", "old_version"],
            )
            all_files = file_list_resp["files"]
            updates = file_list_resp["file_updates"]

            all_files = sorted(all_files, key=lambda x: x["category_id"])

            anything_found = False
            names = []

            for info in d_info:
                info = NexusHandler._update(info, updates, all_files)
                anything_found = False
                if sync:
                    json_log_state(
                        "syncing_mod_data",
                        "archive_init",
                        mod=mod.name,
                        archive=info.file_name,
                    )

                for x in all_files:
                    # TODO NAMES ARE NOT UNIQUE :(
                    if info.nexus_file_id and info.nexus_file_id != x["file_id"]:
                        continue

                    if (
                        x["name"].casefold() == info.file_name.casefold()
                        and x["name"] not in names
                    ):
                        info.content_link = x["content_preview_link"]
                        anything_found = True
                        # some mods like pfp have the same archive in
                        # different formats (.zip, .7z, etc)
                        names.append(x["name"])
                        # TODO this may mean you install a newer version than synced...
                        if not sync:
                            info = self.get_link(mod, x, info)
                        elif not nosync:
                            hash_digest = None
                            hash_algo = None
                            virus_url = x.get("external_virus_scan_url", "") or ""
                            m = re.match(
                                r"https://www.virustotal.com/gui/file/(.*?)/.*",
                                virus_url,
                            )
                            if m:
                                hash_digest = m.group(1)
                                hash_algo = "sha256"

                            cache.cache_synced(mod, info, "nexus_data", x)
                            cache.cache_synced(
                                mod,
                                info,
                                "generic_data",
                                cache.generic_data(
                                    x["version"],
                                    size=x["size_in_bytes"],
                                    size_kb=x["size_kb"],
                                    hash_digest=hash_digest,
                                    hash_algo=hash_algo,
                                ),
                            )
                            json_log_state(
                                "syncing_mod_data",
                                "archive_done",
                                mod=mod.name,
                                archive=info.file_name,
                            )
                        dl_data.append(info)
                        break

                if not anything_found:
                    cache.cache_synced(
                        mod, info, "generic_data", cache.generic_data(None)
                    )

                    logger.error(
                        f'No mod file found for "{mod.name}/{info.file_name}" - MOMW'
                        " data may be out of date :( - skipping"
                    )

        return dl_data

    def get_link(self, mod: ModDesc, f, info: DownloadInfo):
        file_id = f["file_id"]
        mod_id = mod.nexus_id
        name = f["name"]
        user_url = f"https://www.nexusmods.com/morrowind/mods/{mod_id}?tab=files&file_id={file_id}&nmm=1"
        info.file_name = name
        info.direct_download = user_url
        info.nexus_file_id = file_id

        if mod_already_exists(mod, info) or mod_archive_exists(mod, info):
            return info

        cached_url = cache.get_nexus_dl_url(mod, info)
        if cached_url:
            m = re.match(r".*&expires=(.+)&.*", cached_url)
            if m:
                expires = int(m.group(1))
                now = int(time.time())
                if now < expires:
                    info.direct_download = cached_url
                    return info

        if Config.NEXUS_PREMIUM:
            # for speed up we get the link later
            def l():
                links = self.nxm.mod_file_download_link("morrowind", mod_id, file_id)
                cache.cache_nexus_dl_url(mod, info, links[1]["URI"])
                return links[1]["URI"]

            info.direct_download = l
            return info

        if Config.VERBOSE:
            logger.info("Opened URL: " + user_url)

        if not Config.NO_GUI and not self.download_link:
            helper.open_url(user_url)
        elif Config.NO_GUI and not self.download_link:
            with open(".umo-url", "wb") as f:
                f.write(user_url.encode())

        key = None
        expires = None
        regex = re.compile(
            r"nxm://morrowind/mods/"
            + str(mod_id)
            + r"/files/"
            + str(file_id)
            + r"\?key=(.*)&expires=(.*)&.*"
        )
        while not key:
            s = ""
            if self.download_link:
                s = self.download_link
            elif Config.SOCKET_SERVER:
                s = read_socket(lambda x: re.search(regex, x))
            else:
                s = input("> ")

            m = re.search(regex, s)
            if m:
                key = m.group(1)
                expires = m.group(2)
            else:
                if Config.SOCKET_SERVER:
                    logger.debug(f"That didn't work. Opening url again.")
                    if not Config.NO_GUI:
                        helper.open_url(user_url)
                else:
                    logger.error(f"That didn't work. You provided {s} - Try again:")

        links = self.nxm.mod_file_download_link(
            "morrowind", mod_id, file_id, key, expires
        )
        info.direct_download = links[0]["URI"]
        info.nexus_file_id = file_id
        cache.cache_nexus_dl_url(mod, info, links[0]["URI"])

        return info

    def _sync(self, mod: ModDesc):
        self.download_links(mod)

    def prepare(self, mod: ModDesc):
        return mod

    def _handle_nexus_dl(self, mod: ModDesc, genfn, clean=False, lazy=False):
        later = []
        forceall = False

        for data in mod.download_info:
            if not mod_already_exists(mod, data):
                forceall = True
                break

        if not forceall:
            return []

        mod_id = mod.nexus_id
        file_list_resp = self.get_file_list(
            mod, mod_id, ["main", "update", "optional", "miscellaneous", "old_version"]
        )
        all_files = file_list_resp["files"]
        updates = file_list_resp["file_updates"]
        all_files = sorted(all_files, key=lambda x: x["category_id"])

        # TODO refactor me
        for data in mod.download_info:
            data = NexusHandler._update(data, updates, all_files)

            for x in all_files:
                if data.nexus_file_id and data.nexus_file_id != x["file_id"]:
                    continue

                if x["name"].casefold() == data.file_name.casefold():
                    data = self.get_link(mod, x, data)
                    break

            fn = genfn(mod, data)
            yield fn

    def download(self, mod: ModDesc):
        def gen(_mod, _data):
            def d():
                try:
                    nonlocal _mod
                    nonlocal _data
                    base = f"{Config.BASEPATH}/{_mod.category}/"
                    download(_mod, _data)
                except Exception as ex:
                    logger.error(f"{_mod.name} error occured - skipping: {ex}")
                    json_log_state("downloading", "error", f"{ex}", mod=_mod.name)
                    if Config.VERBOSE:
                        traceback.print_exc()

            return d

        return self._handle_nexus_dl(mod, gen)


HANDLERS = {
    "tr": TRHandler,
    "nexus": NexusHandler,
    "gitlab": MOMWGitlabHandler,
    "raw-gitlab": GitlabHandler,
    "github": GithubHandler,
    "baturin": BaturinHandler,
    "googledrive": GoogleDriveHandler,
    "direct": DirectHandler,
}
