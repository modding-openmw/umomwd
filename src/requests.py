import json
from io import BytesIO

import certifi
import pycurl

from config import Config

CAINFO = certifi.where()


class Response:
    def __init__(self, content, headers):
        self._content = content
        self._headers = headers

    @property
    def content(self):
        return self._content

    @property
    def headers(self):
        header_dict = {}
        for header in self._headers.split("\r\n"):
            if ": " in header:
                key, value = header.split(": ", 1)
                header_dict[key] = value
        return header_dict

    def json(self):
        try:
            return json.loads(self._content.decode())
        except json.JSONDecodeError:
            raise ValueError("Response content is not valid JSON")


def _make_request(url, method, data=None, headers=None):
    buffer = BytesIO()
    header_buffer = BytesIO()
    curl = pycurl.Curl()

    curl.setopt(curl.CAINFO, CAINFO)
    curl.setopt(curl.URL, url)
    curl.setopt(curl.WRITEDATA, buffer)
    curl.setopt(curl.HEADERFUNCTION, header_buffer.write)
    curl.setopt(curl.CUSTOMREQUEST, method)
    curl.setopt(curl.FOLLOWLOCATION, 1)
    curl.setopt(curl.HTTPHEADER, Config.CURL_HEADER)
    # curl.setopt(curl.VERBOSE, 1)

    if headers:
        curl.setopt(curl.HTTPHEADER, [f"{k}: {v}" for k, v in headers.items()])

    if method == "POST" and data:
        curl.setopt(curl.POSTFIELDS, data)

    curl.perform()
    curl.close()

    body = buffer.getvalue()
    headers = header_buffer.getvalue().decode()

    return Response(body, headers)


def get(url, headers=None):
    return _make_request(url, "GET", headers=headers)


def post(url, data=None, headers=None):
    return _make_request(url, "POST", data=data, headers=headers)
