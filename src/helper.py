# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import platform
import re
import subprocess
import sys
import webbrowser

from config import Config
from log import get_logger
from momw_types import ModDesc

logger = get_logger()


def where(mod: ModDesc, download_info=None):
    if download_info:
        return f"{Config.BASEPATH}/{mod.category}/{download_info.extract_to}"
    else:
        return f"{Config.BASEPATH}/{mod.category}/{mod.dir}"


def slugify(text: str):
    if text:
        return re.sub(r"\s+", "-", text.lower().strip())
    else:
        return text


def sanitize(name):
    s = name
    for i in [" ", "-", "_", "(", ")", "'", ".", ":"]:
        s = s.replace(i, "")
    return s


def download_paths(mod_name, archive_name, version=None):
    s = sanitize(mod_name)
    a = sanitize(archive_name)
    cache_dir = Config.CACHE_DIR

    directory = f"{cache_dir}/archive-cache/{s}"
    archive = f"{cache_dir}/archive-cache/{s}/{a}.archive"
    if version:
        archive = f"{cache_dir}/archive-cache/{s}/{a}_{version}.archive"

    return directory, archive


def take_n_gen(l, n):
    while len(l) > 0:
        res = l[:n]
        l = l[n:]
        yield res


# https://stackoverflow.com/questions/1094841/get-a-human-readable-version-of-a-file-size
def sizeof_fmt(num, suffix="B"):
    for unit in ("", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"):
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"


def open_url(url):
    if platform.system() == "Darwin":
        subprocess.call(["open", url])
    elif platform.system() == "Windows":
        os.startfile(url)
    else:
        subprocess.call(["xdg-open", url])


def get_real_argv0():
    res = sys.argv[0]

    if os.environ.get("APPIMAGE"):
        logger.debug(f"ARGV0: got from env -> APPIMAGE")
        res = os.environ.get("APPIMAGE")

    if os.environ.get("NUITKA_ORIGINAL_ARGV0"):
        logger.debug(f"ARGV0: got from env")
        res = os.environ.get("NUITKA_ORIGINAL_ARGV0")

    res = os.path.realpath(res)

    if res.endswith(".py"):
        exe = os.path.abspath(sys.executable)
        if platform.system() == "Windows":
            res = f'"{exe}" "{res}"'
        else:
            exe = exe.replace(" ", "\\ ")
            res = res.replace(" ", "\\ ")
            res = f"{exe} {res}"
    else:
        if platform.system() == "Windows":
            res = f'"{res}"'
        else:
            res = res.replace(" ", "\\ ")

    logger.debug(f"ARGV0: {res}")
    return res
