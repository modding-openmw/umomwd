# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import hashlib
import os
import subprocess
import sys
from io import BytesIO
from pathlib import Path

import certifi
import curl
import pycurl
import rarfile
import tenacity

import cache
import mega
import umocurldl
from config import Config
from helper import download_paths, sanitize, where
from log import get_logger, json_log_state
from momw_types import DownloadInfo, ModDesc
from notification import send_notification
from pseudomagic import is_archive, is_html

logger = get_logger()

CAINFO = certifi.where()
WTF = [
    # wrong hash found for some of the files in Morrowind Enhanced Textures
    "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
]


@tenacity.retry(
    stop=tenacity.stop_after_attempt(5),
    wait=tenacity.wait_fixed(2),
    retry=tenacity.retry_if_exception_type(pycurl.error),
)
def curl_get_body(url, headers=[], resp_header: BytesIO = None, statusfn=None):
    url = url.replace(" ", "%20")
    buffer = BytesIO()

    c = pycurl.Curl()
    c.setopt(c.URL, url)
    c.setopt(c.WRITEDATA, buffer)
    c.setopt(c.CAINFO, CAINFO)
    c.setopt(c.HTTPHEADER, Config.CURL_HEADER + headers)
    if resp_header:
        c.setopt(c.HEADERFUNCTION, resp_header.write)
    c.setopt(c.FOLLOWLOCATION, 1)
    # c.setopt(c.VERBOSE, 1 if Config.VERBOSE else 0)
    c.perform()
    status_code = c.getinfo(c.HTTP_CODE)
    c.close()

    body = buffer.getvalue().decode("utf8")
    if statusfn:
        statusfn(status_code, body)
    else:
        if status_code not in (200, 201):
            raise Exception(f"HTTP call failed - got {status_code}: {body}")

    return body


@tenacity.retry(
    stop=tenacity.stop_after_attempt(5),
    wait=tenacity.wait_fixed(2),
    retry=tenacity.retry_if_exception_type(pycurl.error),
)
def curl_get_content_length(url):
    url = url.replace(" ", "%20")
    c = curl.Curl()
    c.set_option(pycurl.CAINFO, CAINFO)
    c.set_option(pycurl.HTTPHEADER, Config.CURL_HEADER)
    # c.set_option(pycurl.VERBOSE, 1 if Config.VERBOSE else 0)
    c.set_option(pycurl.FOLLOWLOCATION, True)
    c.head(url)
    raw_hdr = c.header()
    parsed = list(map(lambda x: x.split(": ", maxsplit=1), raw_hdr.split("\r\n")[1:]))
    parsed.reverse()
    for x in parsed:
        if x[0].lower() == "content-length":
            return int(x[1])
    return None


def curl_download_file(
    url, dest, size=None, digests=None, fake_user_agent=False, mod=None, archive=None
):
    url = url.replace(" ", "%20")

    def myconfigure(c):
        c.setopt(pycurl.CAINFO, CAINFO)
        if Config.MAX_SPEED:
            c.setopt(
                pycurl.MAX_RECV_SPEED_LARGE, int(Config.MAX_SPEED / Config.MAX_THREADS)
            )
        if fake_user_agent:
            c.setopt(
                c.HTTPHEADER,
                [
                    "User-Agent: Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.11"
                    " (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11"
                ],
            )
        else:
            c.setopt(c.HTTPHEADER, Config.CURL_HEADER)
        return c

    dl = umocurldl.UmomwdCurldl(
        basedir=os.path.dirname(dest),
        progress=True,
        curl_config_callback=myconfigure,
        timeout_sec=60 * 60,
        retry_attempts=15,
    )

    dl.mod = mod
    dl.archive = archive

    dl.get(url=url, rel_path=os.path.basename(dest), size=size, digests=digests)


def extract_python(archive, directory):
    # this only works for rar files
    Path(directory).mkdir(parents=True, exist_ok=True)

    f = rarfile.RarFile(archive)
    f.extractall(path=directory)

    if len(os.listdir(directory)) == 0:
        # we don't want to keep broken archives, so delete it
        os.unlink(archive)
        raise Exception(
            "extraction failed - probably a broken archive, try rerunning install"
        )


def extract(mod: ModDesc, data: DownloadInfo):
    json_log_state("extracting", "archive_init", mod=mod.name, archive=data.file_name)
    c = cache.get_installed(mod, data)
    s = cache.get_synced(mod, data)

    if not c:
        c = {}

    if not c.get("downloaded"):
        json_log_state(
            "extracting",
            "archive_info",
            payload="not downloaded",
            mod=mod.name,
            archive=data.file_name,
        )
        return

    directory = where(mod, data)

    d, a = download_paths(
        mod.name, data.file_name, s.get("generic_data", {}).get("version")
    )

    # sys.stderr.write(f"{a} -> {directory}\n")
    # sys.stderr.flush()

    cmd = [
        Config.BIN_7Z,
        "x",
        "-bso0",
        "-bse2",
        "-bsp2",
        "-y",
        "-w",
        f"-o{directory}",
        a,
    ]
    proc = None
    if Config.VERBOSE:
        proc = subprocess.run(cmd)
    else:
        proc = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    if proc.returncode != 0:
        if rarfile.is_rarfile(a):
            logger.debug(
                f"{a} Something bad happened during extraction with 7z - trying to"
                " fallback"
            )
            extract_python(a, directory)
        else:
            os.unlink(a)
            raise Exception("extraction failed")

    cache.cache_installed(mod, data, "extracted", True)
    cache.cache_installed(mod, data, "generic_data", s.get("generic_data"))
    json_log_state("extracting", "archive_done", mod=mod.name, archive=data.file_name)


def download(mod: ModDesc, data: DownloadInfo):
    c = cache.get_installed(mod, data)
    s = cache.get_synced(mod, data)

    if not c:
        c = {}

    url = data.direct_download

    if callable(data.direct_download):
        url = data.direct_download()

    directory = where(mod, data)

    d, a = download_paths(
        mod.name, data.file_name, s.get("generic_data", {}).get("version")
    )
    archive = Path(a)

    if not archive.exists() or not c.get("downloaded"):
        json_log_state("downloading", "init", mod=mod.name, archive=data.file_name)
        send_notification("UMO", f"Downloading {data.file_name}...")
        Path(d).mkdir(parents=True, exist_ok=True)  # make dir
        size_in_bytes = s.get("generic_data", {}).get("size_in_bytes")
        hash_algo = s.get("generic_data", {}).get("hash", {}).get("type")
        hash_digest = s.get("generic_data", {}).get("hash", {}).get("value")
        digests = None
        if hash_algo and hash_digest and hash_digest not in WTF:
            digests = {}
            digests[hash_algo] = hash_digest
        else:
            if hash_digest in WTF:
                logger.debug(f"WTF -> {mod.name}/{data.file_name}")
            else:
                logger.debug(f"NO HASH -> {mod.name}/{data.file_name}")

        if not size_in_bytes:
            if Path(a + ".part").exists():
                os.unlink(a + ".part")

        # mega files need to be downloaded and extracted
        if url.startswith("https://mega.nz/"):
            mega.download_mega(
                url,
                a,
                lambda url, path: curl_download_file(
                    url, path, mod=mod.name, archive=data.file_name
                ),
            )
        else:
            if mod.handler == "github":
                curl_download_file(
                    url,
                    a,
                    size_in_bytes,
                    digests,
                    True,
                    mod=mod.name,
                    archive=data.file_name,
                )
            else:
                curl_download_file(
                    url, a, size_in_bytes, digests, mod=mod.name, archive=data.file_name
                )

        if not is_archive(archive):
            err = "Downloaded file is not an archive!"
            if is_html(archive):
                from bs4 import BeautifulSoup

                with open(archive, "rb") as htmlf:
                    html = htmlf.read()
                    soup = BeautifulSoup(html, features="html.parser")
                    err = "HTML received instead of archive: " + soup.get_text("\n")
            os.unlink(archive)
            raise Exception(err)

        cache.cache_installed(mod, data, "downloaded", True)
        if not digests:
            if not size_in_bytes:
                size_in_bytes = os.path.getsize(archive)

            synced = cache.get_synced(mod, data)["generic_data"]
            hash_algo = "sha256"

            hash_obj = hashlib.new(hash_algo)
            with open(archive, "rb") as path_obj:
                while chunk := path_obj.read(1024 * 1024 * 32):
                    hash_obj.update(chunk)

            cache.cache_synced(
                mod,
                data,
                "generic_data",
                cache.generic_data(
                    synced["version"],
                    size=size_in_bytes,
                    hash_digest=hash_obj.hexdigest().lower(),
                    hash_algo=hash_algo,
                    url=synced["url"],
                ),
            )

        json_log_state("downloading", "done", mod=mod.name, archive=data.file_name)
