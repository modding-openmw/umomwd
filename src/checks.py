# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import configparser
import os
import platform
import subprocess
import sys

import helper


def umo_is_default_nxm_handler():
    try:
        proc = subprocess.run(
            ["xdg-mime", "query", "default", "x-scheme-handler/nxm"],
            capture_output=True,
        )
        if proc.stdout.startswith(b"umo.desktop"):
            return True
    except:
        return False
    return False


def desktop_file_ok():
    binary = helper.get_real_argv0()
    try:
        c = configparser.ConfigParser(interpolation=None)
        desktop_file = os.path.expanduser("~/.local/share/applications/umo.desktop")
        c.read(desktop_file)
        if c["Desktop Entry"]["Exec"].startswith(binary):
            return True
    except:
        return False
    return False


def nxm_handler_set_up():
    binary = helper.get_real_argv0().replace("python.exe", "pythonw.exe")
    try:
        if platform.system() == "Darwin":
            return True
        elif platform.system() == "Windows":
            import winreg

            with winreg.OpenKey(
                winreg.HKEY_CURRENT_USER,
                r"Software\Classes\nxm\shell\open",
                0,
                winreg.KEY_ALL_ACCESS,
            ) as key:
                command = winreg.QueryValue(key, "command")
                if binary in str(command):
                    return True
            return False
        else:
            return umo_is_default_nxm_handler() and desktop_file_ok()
    except:
        return False

    return False


def running_as_admin():
    if platform.system() == "Windows":
        import ctypes

        return ctypes.windll.shell32.IsUserAnAdmin() != 0
    else:
        return os.getuid() == 0
