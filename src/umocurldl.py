# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import math
import os
import time
from typing import Callable

import curldl
import pycurl
import tqdm

from clrs import colour_generator
from config import Config
from log import json_log_state

log = logging.getLogger(__name__)


class UmomwdCurldl(curldl.Curldl):

    DOWNLOAD_RETRY_ERRORS = {
        pycurl.E_COULDNT_RESOLVE_PROXY,
        pycurl.E_COULDNT_RESOLVE_HOST,
        pycurl.E_COULDNT_CONNECT,
        pycurl.E_FTP_ACCEPT_FAILED,
        pycurl.E_FTP_ACCEPT_TIMEOUT,
        pycurl.E_FTP_CANT_GET_HOST,
        pycurl.E_HTTP2,
        pycurl.E_PARTIAL_FILE,
        pycurl.E_FTP_PARTIAL_FILE,
        pycurl.E_HTTP_RETURNED_ERROR,
        pycurl.E_OPERATION_TIMEDOUT,
        pycurl.E_FTP_PORT_FAILED,
        pycurl.E_SSL_CONNECT_ERROR,
        pycurl.E_TOO_MANY_REDIRECTS,
        pycurl.E_GOT_NOTHING,
        pycurl.E_SEND_ERROR,
        pycurl.E_RECV_ERROR,
        pycurl.E_SSH,
        pycurl.E_ABORTED_BY_CALLBACK,
    }

    colour = colour_generator()

    def _download_partial(
        self,
        url: str,
        path: str,
        *,
        timestamp: int | float | None = None,
        description: str | None = None,
    ) -> None:
        """Start or resume a partial download of a URL to resolved path.
        If timestamp of an already downloaded file is provided, remove the partial file
        if the URL content is not more recent than the timestamp. This method should be
        invoked with a retry policy.

        :param url: URL to download
        :param path: resolved path of a partial download file
        :param timestamp: last-modified timestamp of an already downloaded ``path``,
            if it exists
        :param description: description string for progress bar
            (e.g., base name of downloaded file)
        :raises pycurl.error: PycURL error when downloading, may result in a retry
            according to policy
        """
        curl, initial_size = self._get_configured_curl(url, path, timestamp=timestamp)

        def log_partial_download(
            message_prefix: str, *, error: pycurl.error | None = None
        ) -> None:
            """Log information about partially downloaded file at ``INFO`` or ``ERROR``
            log level.

            :param message_prefix: log message prefix
            :param error: PycURL exception, implies ``ERROR`` log level
            """
            if log.isEnabledFor(log_level := logging.ERROR if error else logging.INFO):
                log.log(
                    log_level,
                    message_prefix
                    + f" {path} {initial_size:,} -> {os.path.getsize(path):,} B"
                    f" ({self._get_response_status(curl, url, error)})"
                    f" [{curldl.util.Time.timestamp_delta(curl.getinfo(pycurl.TOTAL_TIME))}]",
                )

        try:
            with (
                open(path, "ab") as path_stream,
                tqdm.tqdm(
                    unit="B",
                    unit_scale=True,
                    unit_divisor=1024,
                    miniters=1,
                    desc=description,
                    disable=Config.JSON_OUTPUT,
                    leave=False,
                    dynamic_ncols=True,
                    colour=next(self.colour),
                    initial=initial_size,
                    bar_format=(
                        "{desc:<30.29}|{bar}| {n_fmt:>5.5}/{total_fmt:<5.5}"
                        " [{rate_fmt:>8.8}]"
                    ),
                ) as progress_bar,
            ):
                progress_bar.mod = self.mod
                progress_bar.archive = self.archive
                self._perform_curl_download(curl, path_stream, progress_bar)

        except pycurl.error as ex:
            if not ex.args[0] in self.DOWNLOAD_RETRY_ERRORS or Config.VERBOSE:
                log_partial_download("Download interrupted", error=ex)
            if ex.args[1].endswith("416"):
                self._discard_file(path, force_remove=True)
            else:
                self._discard_file(path)
            raise

        if curl.getinfo(pycurl.CONDITION_UNMET):
            log.info("Discarding %s because it is not more recent", path)
            self._discard_file(path, force_remove=True)
            return

        log_partial_download("Finished downloading")
        curldl.util.FileSystem.set_file_timestamp(
            path, curl.getinfo(pycurl.INFO_FILETIME)
        )

    def _perform_curl_download(
        self, curl: pycurl.Curl, write_stream, progress_bar
    ) -> None:
        """Complete pycurl.Curl configuration and start downloading.

        :param curl: configured :class:`pycurl.Curl` instance
        :param write_stream: output stream to write to (a file opened in binary write
            mode)
        :param progress_bar: progress bar to use; :func:`XFERINFOFUNCTION` is configured
            if enabled
        """
        curl.setopt(pycurl.WRITEDATA, write_stream)

        curl.setopt(
            pycurl.XFERINFOFUNCTION, self._get_curl_progress_callback(progress_bar)
        )
        curl.setopt(pycurl.NOPROGRESS, False)

        curl.perform()

    @staticmethod
    def _get_curl_progress_callback(
        progress_bar,
    ) -> Callable[[int, int, int, int], None]:
        """Constructs a progress bar-updating callback for :func:`XFERINFOFUNCTION`.

        :param progress_bar: progress bar to use, must be enabled
        :return: :func:`XFERINFOFUNCTION` callback
        """
        last_receive_time = None
        last_size = None
        last_log_size = 0

        def curl_progress_cb(
            download_total: int, downloaded: int, upload_total: int, uploaded: int
        ) -> None:
            """Progress callback for :func:`XFERINFOFUNCTION`, only called
            if :data:`pycurl.NOPROGRESS` is ``0``.

            :param download_total: total bytes to download; initial file size is not
                included if resuming; equal to ``0`` when download is just being started
                and download size is not yet available
            :param downloaded: bytes downloaded so far; initial file size is not
                included if resuming
            :param upload_total: unused
            :param uploaded: unused
            """
            nonlocal last_size
            nonlocal last_log_size
            nonlocal last_receive_time
            if last_size != downloaded:
                last_receive_time = int(time.time())
            else:
                if int(time.time()) - last_receive_time > 15:
                    return -1

            if not progress_bar.disable:
                if download_total != 0:
                    progress_bar.total = download_total + progress_bar.initial
                progress_bar.update(downloaded + progress_bar.initial - progress_bar.n)

            if last_size != downloaded and downloaded > 0:
                _percent_last = (
                    download_total / last_log_size if last_log_size > 0 else 0
                )
                _percent_cur = download_total / downloaded

                percent_last = 100 / _percent_last if _percent_last > 0 else 0
                percent_cur = 100 / _percent_cur if _percent_cur > 0 else 0

                if int(percent_last) < int(percent_cur):
                    json_log_state(
                        "downloading",
                        "status",
                        mod=progress_bar.mod,
                        archive=progress_bar.archive,
                        n=downloaded,
                        total=download_total,
                    )
                    last_log_size = downloaded
            last_size = downloaded

        return curl_progress_cb
