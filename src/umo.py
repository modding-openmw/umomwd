#!/usr/bin/env python

# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import base64
import getpass
import json
import os
import platform
import shutil
import socket
import stat
import sys
from pathlib import Path

import pwinput

import checks
import icon
import sso
from config import Config, config_dir, config_file
from log import get_logger, set_level
from notification import send_notification

logger = get_logger()


def load_config():
    config_path = config_file()
    with open(config_path, "rb") as f:
        c = json.load(f)
        Config.NEXUS_API_KEY = os.environ.get("UMO_NEXUS_API_KEY", c["NEXUS_API_KEY"])
        Config.TES3CMD = os.environ.get("UMO_TES3CMD", c["TES3CMD"])
        Config.MORROWIND_DIR = os.environ.get("UMO_MORROWIND_DIR", c["MORROWIND_DIR"])
        Config.BASEPATH = os.environ.get("UMO_BASEPATH", c["BASEPATH"])
        Config.CACHE_DIR = os.environ.get(
            "UMO_CACHE_DIR", c.get("CACHE_DIR", Config.CACHE_DIR)
        )


def write_check(path):
    p = path + "/.writetest"
    try:
        Path(path).mkdir(exist_ok=True)
        with open(p, "wb") as f:
            f.write(b"test")
        os.unlink(p)
    except:
        return False
    return True


def check_config():
    Path(config_dir()).mkdir(parents=True, exist_ok=True)
    with open(os.path.join(config_dir(), "openmw.ico"), "wb") as f:
        f.write(base64.b64decode(icon.icon))

    config_path = config_file()
    if not Path(config_path).exists():
        import helper

        nexus_key = sso.get_nexus_key()
        mw_dir = None
        while not mw_dir or not Path(os.path.join(mw_dir, "Data Files")).exists():
            mw_dir = os.path.expanduser(
                input("Please enter the path to a Morrowind installation: ")
            )
            loc = mw_dir.find("Data Files")
            if loc > -1:
                mw_dir = os.path.abspath(mw_dir[0 : loc - 1])

            # basedata is okay and comes from the openmw wizard - see
            # https://gitlab.com/modding-openmw/umo/-/issues/60
            if (
                os.path.basename(os.path.dirname(mw_dir)) == "basedata"
                and mw_dir.exists()
            ):
                break

            if mw_dir and (not Path(os.path.join(mw_dir, "Data Files")).exists()):
                print(f"{mw_dir} doesn't look like a Morrowind installation dir")

        # first try to find tes3cmd ourselves
        tes3cmd = None
        tes3cmd_bin = "tes3cmd"
        if platform.system() == "Windows":
            tes3cmd_bin = "tes3cmd.exe"
        tes3cmd_guess_current_path = os.path.abspath(".") + "/" + tes3cmd_bin
        tes3cmd_guess_parent_dir = os.path.abspath("..") + "/" + tes3cmd_bin
        tes3cmd_guess_same_path_as_umo = (
            os.path.dirname(helper.get_real_argv0()) + "/" + tes3cmd_bin
        )

        # in path?
        if shutil.which("tes3cmd"):
            tes3cmd = "tes3cmd"
            print("found tes3cmd in path")
        elif shutil.which(tes3cmd_guess_current_path):
            tes3cmd = tes3cmd_guess_current_path
            print(f"found tes3cmd at {tes3cmd_guess_current_path}")
        elif shutil.which(tes3cmd_guess_same_path_as_umo):
            tes3cmd = tes3cmd_guess_same_path_as_umo
            print(f"found tes3cmd at {tes3cmd_guess_same_path_as_umo}")
        elif shutil.which(tes3cmd_guess_parent_dir):
            tes3cmd = tes3cmd_guess_parent_dir
            print(f"found tes3cmd at {tes3cmd_guess_parent_dir}")
        else:
            # and if not let the user give us the path
            tes3cmd_txt = """Please enter the path to a tes3cmd binary:
Download from here if you do not have: https://modding-openmw.com/mods/tes3cmd/
"""
            tes3cmd = os.path.expanduser(input(tes3cmd_txt))
            while not shutil.which(tes3cmd):
                print("tes3cmd is either not found or not executable - try again")
                tes3cmd = os.path.expanduser(input(tes3cmd_txt))

        basepath = os.path.abspath(
            input("Please enter a path where modlist should be put: ")
        )

        while basepath.startswith("C:\\Program Files") or not write_check(basepath):
            print("Can't write to that path - try again")
            basepath = os.path.abspath(
                input("Please enter a path where modlist should be put: ")
            )

        print(
            "In the next step you can input another cache dir location. The cache"
            " dir is the location where all downloaded archives are stored. The "
            f"default location is {Config.CACHE_DIR}"
        )
        cache_dir = input(f"Cache dir location (leave blank to use the default): ")
        if len(cache_dir) == 0:
            cache_dir = Config.CACHE_DIR
        else:
            cache_dir = os.path.abspath(cache_dir)

        with open(config_path, "wb") as f:
            f.write(
                json.dumps({
                    "NEXUS_API_KEY": nexus_key,
                    "TES3CMD": os.path.abspath(shutil.which(tes3cmd)),
                    "MORROWIND_DIR": os.path.abspath(mw_dir),
                    "BASEPATH": os.path.abspath(basepath),
                    "CACHE_DIR": os.path.abspath(cache_dir),
                }).encode("utf8")
            )
        os.chmod(config_path, stat.S_IRUSR | stat.S_IWUSR)
        logger.info(f"{config_path} written")
        Config.NEXUS_API_KEY = nexus_key
        Config.TES3CMD = tes3cmd
        Config.CACHE_DIR = cache_dir
        Config.BASEPATH = basepath
        Config.MORROWIND_DIR = mw_dir
    else:
        load_config()


def xdg_fn(args):
    nxm = args.nxmlink.startswith("nxm://")
    momw = args.nxmlink.startswith("momw://")
    if not nxm and not momw:
        send_notification("UMO", "You gave me no nxm or momw link :(")
        return

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    can_connect = False
    try:
        s.connect(Config.SOCKET)
        can_connect = True
    except Exception:
        can_connect = False

    if can_connect:
        try:
            s.send(args.nxmlink.encode())
            r = s.recv(1024).decode()
            if r.startswith("OK"):
                send_notification("UMO", "Sent link to running UMO")
            else:
                send_notification("UMO", f"Sent link to running UMO but said: {r} :O")
        except Exception:
            send_notification("UMO", "Sending link to running UMO did not work :O")
    else:
        try:
            load_config()
            import subcmds

            subcmds.add(args)
        except Exception as ex:
            send_notification("UMO", f"That did not work: {ex}")


def call_subcmd(cmd, args):
    check_config()

    import subcmds

    getattr(subcmds, cmd)(args)


def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument("-V", "--version", action="store_true", help="print version")
    p.set_defaults(
        func=lambda x: (
            sys.stdout.write(Config.APP_NAME + "-" + Config.APP_VER + "\n")
            if x.version
            else sys.stdout.write(
                "Please specify a subcommand (check --help for options)\n"
            )
        )
    )
    subparsers = p.add_subparsers(title="subcommands")

    check = subparsers.add_parser(
        "check", help="check if umo has all necessary dependencies"
    )
    check.set_defaults(func=lambda x: call_subcmd("check", x))

    setup = subparsers.add_parser(
        "setup", help="setup umo as the default nxm link handler"
    )
    setup.set_defaults(func=lambda x: call_subcmd("setup", x))

    info = subparsers.add_parser(
        "info", help="print info about the directories umo will use"
    )
    info.set_defaults(func=lambda x: call_subcmd("info", x))

    add = subparsers.add_parser("add", help="add a mod to the custom modlist")
    add.add_argument("-m", "--modlist", help="add to this modlist instead")
    add.add_argument("nxmlink")
    add.set_defaults(func=lambda x: call_subcmd("add", x))

    remove = subparsers.add_parser(
        "remove", help="remove a mod from the custom modlist"
    )
    remove.add_argument("-m", "--modlist", help="remove from this modlist instead")
    remove.add_argument("mod")
    remove.set_defaults(func=lambda x: call_subcmd("remove", x))

    install = subparsers.add_parser("install", help="install a modlist")

    reconfig = subparsers.add_parser(
        "reconfig", help="wipe the configuration file and re-prompt for values"
    )

    install.add_argument("modlist")
    install.add_argument(
        "-v", "--verbose", action="store_true", help="Print more stuff"
    )

    install.add_argument(
        "-P",
        "--nexus-premium",
        action="store_true",
        help="Force nexus-premium handling",
    )

    install.add_argument(
        "-p",
        "--no-nexus-premium",
        action="store_true",
        help="Force non nexus-premium handling",
    )

    install.add_argument(
        "-n", "--no-notifications", action="store_true", help="Don't use notifications!"
    )

    install.add_argument(
        "-s",
        "--use-socket",
        action="store_true",
        help="DEPRECATED: the socket is ALWAYS opened even if not using this flag",
    )

    install.add_argument(
        "-G",
        "--no-gui",
        action="store_true",
        help="Don't call anything that will break in a non gui env",
    )

    install.add_argument(
        "-e",
        "--download-only",
        action="store_true",
        help="Only download archives don't extract",
    )

    install.add_argument(
        "-S",
        "--subset",
        help=(
            "Only install this subset of mods - use comma-seperated folder names (e.g."
            " PatchforPurists,MonochromeUserInterface)"
        ),
    )

    install.add_argument(
        "-K",
        "--skip",
        help=(
            "Skip this subset of mods - use comma-seperated folder names (e.g."
            " PatchforPurists,MonochromeUserInterface)"
        ),
    )

    install.add_argument(
        "-T",
        "--skip-tags",
        help=(
            "Skip subset of mods which have these tags - use comma-seperated slug tag"
            " names (e.g. high-res,vanilla-friendly)"
        ),
    )

    install.add_argument(
        "-t",
        "--threads",
        help="number of max threads to use for downloading and extraction",
    )

    install.add_argument(
        "-l",
        "--limit-speed",
        help="limit download speed in MebiBytes per second",
    )

    install.set_defaults(func=lambda x: call_subcmd("install", x))

    cache = subparsers.add_parser("cache", help="cache stuff")
    csub = cache.add_subparsers(title="subcommands")

    #
    sync = csub.add_parser("sync", help="resync the cache")
    sync.set_defaults(func=lambda x: call_subcmd("cache_sync", x))
    sync.add_argument("modlist")
    sync.add_argument(
        "-b", "--beta", action="store_true", help="Use beta.modding-openmw.com"
    )
    sync.add_argument(
        "-d",
        "--use-dev-mods",
        action="store_true",
        help="For mods that have development versions - use those",
    )
    sync.add_argument(
        "-M",
        "--momw-url",
        help="URL to some other running MOMW app",
    )
    sync.add_argument("-v", "--verbose", action="store_true", help="Print more stuff")
    sync.add_argument(
        "-S",
        "--subset",
        help=(
            "Only sync this subset of mods - use comma-seperated folder names (e.g."
            " PatchforPurists,MonochromeUserInterface)"
        ),
    )
    sync.add_argument(
        "-K",
        "--skip",
        help=(
            "Skip this subset of mods - use comma-seperated folder names (e.g."
            " PatchforPurists,MonochromeUserInterface)"
        ),
    )
    sync.add_argument(
        "-T",
        "--skip-tags",
        help=(
            "Skip subset of mods which have these tags - use comma-seperated slug tag"
            " names (e.g. high-res,vanilla-friendly)"
        ),
    )
    sync.add_argument(
        "-s",
        "--skip-momw",
        action="store_true",
        help="Don't update momw data",
    )
    sync.add_argument(
        "-t",
        "--threads",
        help="number of max threads to use for downloading",
    )

    print_content = csub.add_parser("print-content-links", help="print out the content links for all nexus mods")
    print_content.set_defaults(func=lambda x: call_subcmd("cache_print_content_links", x))
    print_content.add_argument("modlist")

    print_content.add_argument("-v", "--verbose", action="store_true", help="Print more stuff")
    print_content.add_argument(
        "-S",
        "--subset",
        help=(
            "Only sync this subset of mods - use comma-seperated folder names (e.g."
            " PatchforPurists,MonochromeUserInterface)"
        ),
    )
    print_content.add_argument(
        "-K",
        "--skip",
        help=(
            "Skip this subset of mods - use comma-seperated folder names (e.g."
            " PatchforPurists,MonochromeUserInterface)"
        ),
    )
    print_content.add_argument(
        "-T",
        "--skip-tags",
        help=(
            "Skip subset of mods which have these tags - use comma-seperated slug tag"
            " names (e.g. high-res,vanilla-friendly)"
        ),
    )

    # reset
    reset = csub.add_parser("reset", help="reset local cache completely")
    reset.set_defaults(func=lambda x: call_subcmd("cache_reset", x))
    reset.add_argument("type", choices=["all", "installed", "nexus", "momw"])

    # list
    list_cmd = csub.add_parser("list", help="list cached modlists")
    list_cmd.set_defaults(func=lambda x: call_subcmd("cache_list", x))
    list_cmd.add_argument("type", choices=["installed", "momw"])

    # dump
    dump = csub.add_parser("dump", help="dump modlist")
    dump.set_defaults(func=lambda x: call_subcmd("cache_dump", x))
    dump.add_argument("modlist")

    # dump
    cadd = csub.add_parser("add", help="add a custom modlist")
    cadd.set_defaults(func=lambda x: call_subcmd("cache_add", x))
    cadd.add_argument("file")
    cadd.add_argument(
        "-n",
        "--list-name",
        help="the list name (if not set the filename sans extension will be used)",
    )

    # query
    query = csub.add_parser("query", help="query cache")
    query.add_argument("type", choices=["installed", "nexus", "momw", "synced"])
    query.add_argument("path")
    query.set_defaults(func=lambda x: call_subcmd("cache_query", x))

    # patch
    patch = csub.add_parser(
        "patch", help="patch cache - use this only if you know what you're doing"
    )
    patch.add_argument("type", choices=["installed", "synced", "nexus", "momw"])
    patch.add_argument("path")
    patch.add_argument("value")
    patch.add_argument(
        "-p",
        "--json-path",
        action="store_true",
        help=(
            "value is not interpreted as json but as a json-path just like PATH which"
            " is also operating on the selected cache cache type"
        ),
    )
    patch.set_defaults(func=lambda x: call_subcmd("cache_patch", x))

    # done
    done = csub.add_parser(
        "done",
        help=(
            "set mod to manually done - it will not be processed further but can be"
            " updated later on"
        ),
    )
    done.add_argument("mod_list")
    done.add_argument("mod")
    done.set_defaults(func=lambda x: call_subcmd("cache_done", x))

    # hy repl
    repl = csub.add_parser(
        "repl",
        help=(
            "run a python repl... because... why not?  Bound vars: INSTALLED, SYNCED,"
            " MOMW, NEXUS which represent the current cache"
        ),
    )
    repl.set_defaults(func=lambda x: call_subcmd("cache_repl", x))

    if "UMO_CI" in os.environ:
        ci = subparsers.add_parser("ci", help="for ci tests")
        cisub = ci.add_subparsers(title="subcommands")
        argv0 = cisub.add_parser("argv0", help="print arvg[0]")

        def argv0_fn(args):
            import helper

            print(helper.get_real_argv0())

        argv0.set_defaults(func=argv0_fn)

    return p.parse_args()


class PseudoArgs:
    nxmlink: str = None
    modlist: None = None


def main():
    if os.environ.get("APPIMAGE", False):
        os.environ.pop("LD_LIBRARY_PATH")
        os.environ.pop("LD_PRELOAD")

    if checks.running_as_admin() and not os.environ.get("UMO_ALLOW_ROOT", False):
        logger.error("Don't run umo with admin rights!")
        sys.exit(3)

    # https://docs.python.org/3/library/shutil.html#shutil.which
    os.environ["NoDefaultCurrentDirectoryInExePath"] = (
        "I_have_no_bill_and_I_must_scream"
    )

    if "--verbose" in sys.argv or "-v" in sys.argv:
        import logging

        Config.VERBOSE = True
        set_level(logging.DEBUG)
        logging.getLogger("curldl").setLevel(logging.INFO)
        logging.getLogger("umocurldl").setLevel(logging.INFO)

    if platform.system() == "Windows":
        # *sigh* reload path from registry ... because ...
        import winreg

        try:
            with winreg.OpenKey(
                winreg.HKEY_CURRENT_USER,
                "Environment",
                0,
                winreg.KEY_ALL_ACCESS,
            ) as key:
                orig_path, _ = winreg.QueryValueEx(key, "Path")
                logger.debug(f"REG PATH: {orig_path}")
                logger.debug(f"ENV PATH: {os.environ['PATH']}")
                os.environ["PATH"] = orig_path + ";" + os.environ["PATH"]
                logger.debug(f"PATH: {os.environ['PATH']}")
        except Exception:
            logger.debug("could not set path")

    if len(sys.argv) > 1 and sys.argv[1] == "reconfig":
        choice = input(
            """You are about to erase the umo configuration file and be re-prompted for all settings.
Are you sure you want to do this? y/n """
        )
        if choice != "y":
            print("Did not receive a 'y' response, exiting...")
            sys.exit(1)
        cfg_file = config_file()
        if os.path.isfile(cfg_file):
            os.remove(cfg_file)
        check_config()
        sys.exit(0)

    if (
        platform.system() == "Darwin"
        and len(sys.argv) == 1
        and "__CFBundleIdentifier" in os.environ
    ):
        # protocol handler for mac os
        # urls are passed by apple event not per parameter

        from Cocoa import NSApp, NSApplication, NSTimer
        from Foundation import NSObject

        class AppDelegate(NSObject):
            def applicationWillFinishLaunching_(self, notification):
                if "UMO_CI_APP_CHECK" in os.environ:
                    os._exit(0)

            def application_openURLs_(self, app, urls):
                for url in urls:
                    self.handle_custom_url(url.absoluteString())

            def handle_custom_url(self, url):
                if url.startswith("nxm://"):
                    args = PseudoArgs()
                    args.nxmlink = url
                    xdg_fn(args)
                    return True

        app = NSApplication.sharedApplication()
        delegate = AppDelegate.alloc().init()
        NSApp.setDelegate_(delegate)
        app.run()
    else:
        if len(sys.argv) == 2:
            if sys.argv[1].startswith("nxm://") or sys.argv[1].startswith("momw://"):
                args = PseudoArgs()
                args.nxmlink = sys.argv[1]
                xdg_fn(args)
                sys.exit(0)

        args = parse_args()
        args.func(args)


if __name__ == "__main__":
    main()
