sevenzip = [b"\x37\x7a\xbc\xaf\x27\x1c"]
zipfile = [b"\x50\x4b\x03\x04", b"\x50\x4b\x05\x06", b"\x50\x4b\x07\x08"]
rar = [b"\x52\x61\x72\x21\x1a\x07\x00", b"\x52\x61\x72\x21\x1a\x07\x01\x00"]
html = [b"<!DOCTYPE html>"]

archive = sevenzip + zipfile + rar


def is_archive(path):
    read = b""
    with open(path, "rb") as a:
        read = a.read(20)
    if any([read.startswith(magic) for magic in archive]):
        return True
    return False


def is_html(path):
    read = b""
    with open(path, "rb") as a:
        read = a.read(20)
    if any([read.startswith(magic) for magic in html]):
        return True
    return False
