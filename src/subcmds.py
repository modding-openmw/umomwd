# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import logging
import os
import signal
import sys
from pathlib import Path

import cache
import checks
import log
import umonxm
from clrs import colour_generator
from config import Config, config_dir, config_file
from helper import get_real_argv0, sanitize, sizeof_fmt, take_n_gen
from log import _print, get_logger, json_log_state
from momw_types import DownloadInfo, ModDesc, yes_or_no
from notification import send_notification

logger = get_logger()

# install


def signal_handler(signal, frame):
    cache.close()
    sys.exit(2)


def log_status(mod_name, current, maximum, text="handling mod"):
    _print("\033[31;1;m{:>3}/{:<3}\033[0m".format(current, maximum))
    if len(text) > 0:
        logger.info(f" {text}: {mod_name}")
    else:
        logger.info(f" {mod_name}")


def get_mods(mod_list):
    import handlers
    import requests

    raw_mods = None
    if not Config.MODLIST_FILE:
        raw_mods = requests.get(
            f"{Config.MODDING_OPENMW_HOST}/lists/{mod_list}/json"
        ).json()
    else:
        with open(mod_list, "rb") as f:
            raw_mods = json.load(f)

    result = []
    manual = []
    dir_lookup = {}

    for mod in raw_mods:
        md = ModDesc.model_validate(mod)

        if md.category in ["settings", "Tools", "FirstSteps"]:
            continue

        handled = False
        dir_lookup[md.dir] = md

        for _, handler in handlers.HANDLERS.items():
            h = handler()
            if h.can(md):
                result.append(md)
                handled = True
                break

        if not handled:
            logger.warning(f"can't handle this mod: {md.name} with url {md.url}")
            manual.append(md)

    for mod in result:
        tmp_conflict_dirs = set()
        for data in mod.download_info:
            for action in data.actions:
                paths = action.get_paths()
                for path in paths:
                    if not path.startswith(mod.dir):
                        logger.debug(
                            f"{mod.name}/{data.file_name} has a conflict to another mod"
                            f" that needs to be patched-> {path}"
                        )
                        tmp_conflict_dirs.add(path.split("/")[0])
        for d in tmp_conflict_dirs:
            dep = dir_lookup.get(d)
            if dep:
                if not mod.conflicts:
                    mod.conflicts = []
                dep.conflicts.append(mod.dir)

    return result, manual


def set_config(args, use_momw=True):
    if args.limit_speed:
        Config.MAX_SPEED = int(args.limit_speed) * 1024 * 1024
    if args.no_notifications:
        Config.NO_NOTIFICATIONS = True
    if args.verbose:
        Config.VERBOSE = True
        log.set_level(logging.DEBUG)
        logging.getLogger("curldl").setLevel(logging.INFO)
        logging.getLogger("umocurldl").setLevel(logging.INFO)
    if args.nexus_premium:
        Config.NEXUS_PREMIUM = True
    if args.no_nexus_premium:
        Config.NEXUS_PREMIUM = False
    if use_momw:
        if args.momw_url:
            Config.MODDING_OPENMW_HOST = args.momw_url
        elif args.beta:
            Config.MODDING_OPENMW_HOST = "https://beta.modding-openmw.com"
    if args.no_gui:
        Config.NO_GUI = True
    if args.download_only:
        Config.DOWNLOAD_ONLY = True
    if args.threads:
        Config.MAX_THREADS = int(args.threads)


def print_nexus_info():
    if Config.JSON_OUTPUT:
        return
    try:
        nxm = umonxm.Nxm(Config.NEXUS_API_KEY)
        premium = nxm.user_details()["is_premium"]
        limits = nxm.get_limits()
        nexus_hour_remaining_req = limits["hour_remaining"]
        nexus_day_remaining_req = limits["day_remaining"]
        nexus_hour_reset_time = limits["hour_reset"]
        nexus_day_reset_time = limits["day_reset"]

        if not premium:
            sys.stderr.write(f"  ┌──── NEXUS INFO ──\n")
            # sys.stderr.write(f"  │ Hourly requests left: {nexus_hour_remaining_req}\n")
            # sys.stderr.write(f"  │ Hourly reset at:      {nexus_hour_reset_time}\n")
            sys.stderr.write(f"  │ Daily requests left:  {nexus_day_remaining_req}\n")
            sys.stderr.write(f"  │ Daily reset at:       {nexus_day_reset_time}\n")
            sys.stderr.write("  └────\n")
    except:
        pass


def install(args):
    import shutil
    import socket
    import stat
    import traceback

    import tqdm

    import handlers

    check(args, True)
    if not checks.nxm_handler_set_up():
        logger.error(
            "The nxm/momw protocol handler is not setup properly.  Please run 'umo"
            " setup' again!"
        )

    signal.signal(signal.SIGINT, signal_handler)

    if not shutil.which(Config.BIN_7Z):
        logger.warning("7z must be in PATH")
        sys.exit(1)

    # premium autodetect
    try:
        nxm = umonxm.Nxm(Config.NEXUS_API_KEY)
        premium = nxm.user_details()["is_premium"]
        Config.NEXUS_PREMIUM = premium
    except Exception as ex:
        logger.error(f"Error during checking nexusmods premium status: {ex}")
        logger.error(f"Either nexusmods has an issue or you API key is borked")

    set_config(args, use_momw=False)
    Config.MODLIST = args.modlist
    Path(Config.BASEPATH).mkdir(parents=True, exist_ok=True)

    real_basepath = Config.BASEPATH
    Config.BASEPATH = f"{Config.BASEPATH}/{Config.MODLIST}"

    # open the socket
    Config.SOCKET_SERVER = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    Config.SOCKET_SERVER.bind(Config.SOCKET)

    import asyncfn  # load after config set

    cache.init()

    mods = []
    manual = []
    if cache.get_data(cache.MOD_CACHE_ID, f"$.{Config.MODLIST}"):
        mods = list(
            map(
                lambda x: ModDesc.validate(x),
                cache.get_data(cache.MOD_CACHE_ID, f"$.{Config.MODLIST}.mods"),
            )
        )
        manual = list(
            map(
                lambda x: ModDesc.validate(x),
                cache.get_data(cache.MOD_CACHE_ID, f"$.{Config.MODLIST}.manual"),
            )
        )
    else:
        sys.stderr.write(f"please run this first: umo cache sync {args.modlist}\n")
        sys.exit(2)

    if args.subset:
        mods = list(filter(lambda x: x.dir in args.subset.split(","), mods))
        manual = list(filter(lambda x: x.dir in args.subset.split(","), manual))
    if args.skip:
        mods = list(filter(lambda x: x.dir not in args.skip.split(","), mods))
        manual = list(filter(lambda x: x.dir not in args.skip.split(","), manual))

    if args.skip_tags:
        mods = list(
            filter(
                lambda x: not any([tag in args.skip_tags.split(",") for tag in x.tags]),
                mods,
            )
        )
        manual = list(
            filter(
                lambda x: not any([tag in args.skip_tags.split(",") for tag in x.tags]),
                manual,
            )
        )

    direct = []
    nexus = []

    size = 0
    for moddesc in mods:
        for info in moddesc.download_info:
            # check if the all mods are synced
            synced = cache.get_synced(moddesc, info)
            if synced.get("generic_data", -1) == -1:
                logger.debug("no generic_data for " + info.file_name)
                sys.stderr.write(
                    "cache not complete - please run this again: umo cache sync"
                    f" {args.modlist}\n"
                )
                sys.exit(2)

            # sum the size overall
            size += cache.synced_size(synced)

    size *= 1.2
    size_extracted = size * 1.85
    full_size = size + size_extracted

    cache_total, cache_used, cache_free = shutil.disk_usage(Config.CACHE_DIR)
    base_total, base_used, base_free = shutil.disk_usage(real_basepath)
    sys.stderr.write(f"  ┌──── SIZE INFO FOR {Config.MODLIST.upper()} ──\n")
    sys.stderr.write(
        f"  │ estimated download size:  {sizeof_fmt(size)} ->"
        f" {sizeof_fmt(cache_free)} free space for {Config.CACHE_DIR}\n"
    )
    sys.stderr.write(
        f"  │ estimated extracted size: {sizeof_fmt(size_extracted)} ->"
        f" {sizeof_fmt(base_free)} free space for {real_basepath}\n"
    )
    sys.stderr.write(f"  │ estimated full size:      {sizeof_fmt(full_size)}\n")
    sys.stderr.write("  │\n")
    sys.stderr.write(f"  │ Make sure your drives have enough space\n")
    sys.stderr.write("  └────\n")
    print_nexus_info()

    # TODO revise this
    # if cache_free != base_free:
    #     if cache_free < size:
    #         logger.error("disk space not sufficient")
    #         sys.exit(1)
    #     if base_free < size_extracted:
    #         logger.error("disk space not sufficient")
    #         sys.exit(1)
    # else:
    #     if base_free < full_size:
    #         logger.error("disk space not sufficient")
    #         sys.exit(1)

    # sort out all mods that are finished
    todo_mods = list(filter(lambda x: not handlers.check_mod_exists(x), mods))
    if len(todo_mods) == 0:
        logger.info("nothing to do - all installed")
        sys.exit(0)

    for mod in mods:
        if mod.handler == "nexus":
            nexus.append(mod)
        else:
            direct.append(mod)

    # for better flow for non premium users sort this
    nexus.sort(
        key=lambda x: cache.synced_size(cache.get_synced(x, x.download_info[0]))
        or 20000000
    )
    # but put a few big ones at the beginning (hopefully not as much
    # as we have threads) so we use as much threads as possible.
    # -> You can't click as fast as the small ones download so you'd only
    # use 1 thread if not a few big ones are in the pipeline
    nexus = (
        list(reversed(nexus[-int(Config.MAX_THREADS / 4) :]))
        + nexus[: -int(Config.MAX_THREADS / 4)]
    )
    maximum = len(mods)
    curr = 0
    clrs = colour_generator()
    logger.info("downloading archives...")
    if not Config.NEXUS_PREMIUM and not Config.NO_GUI:
        logger.warn(
            "For mods hosted on nexusmods your browser will open for each file."
        )
        logger.warn(
            "Click 'slow download' on any opened page to trigger umo to download it."
        )
    prepared = []

    def dlfn(nmod):
        res = handlers.HANDLERS[nmod.handler]()

        try:
            nmod = res.prepare(nmod)
            prepared.append(nmod)
        except Exception as ex:
            logger.error(f"{nmod.name} error occured - skipping: {ex}")
            json_log_state("downloading", "error", f"{ex}", mod=nmod.name)
            if Config.VERBOSE:
                traceback.print_exc()
            return []

        return res.download(nmod)

    total = sum([len(x.download_info) for x in direct + nexus])
    to_dl = list(
        filter(lambda x: not handlers.check_mod_archive_exists(x), direct + nexus)
    )
    rest = total - sum([len(x.download_info) for x in to_dl])
    if len(to_dl) > 0:
        asyncfn.slurp_async(dlfn, to_dl, True, total, rest, None, next(clrs))

    if not Config.DOWNLOAD_ONLY:
        exfns = []

        for nmod in mods:

            def exfngen(m):
                def exfn():
                    nonlocal m
                    try:
                        res = handlers.HANDLERS[m.handler]()
                        m = res.prepare(m)
                        res.extract(m)
                    except Exception as ex:
                        logger.error(f"{m.name} error occured - skipping: {ex}")
                        json_log_state("extracting", "error", f"{ex}", mod=m.name)
                        if Config.VERBOSE:
                            traceback.print_exc()

                return asyncfn.FnLogWrapper(exfn, m.name)

            if not handlers.check_mod_extracted(nmod, mods):
                exfns.append(exfngen(nmod))

        logger.info("extracting downloaded archives... (this can take a while)")
        if len(exfns) > 0:
            asyncfn.run_async(
                exfns, True, len(exfns), None, next(clrs), "extracting", threads=1
            )

        logger.info("running actions...")
        curr = 0
        do_actions_for_these = list(
            filter(lambda x: not handlers.check_mod_actions_done(x), mods)
        )
        for nmod in do_actions_for_these:
            curr += 1
            json_log_state(
                "run_actions", "init", None, curr, len(do_actions_for_these), nmod.name
            )
            res = handlers.HANDLERS[nmod.handler]()
            try:
                nmod = res.prepare(nmod)
                res.run_actions(nmod)
                json_log_state(
                    "run_actions",
                    "done",
                    None,
                    curr,
                    len(do_actions_for_these),
                    nmod.name,
                )
            except Exception as ex:
                logger.error(f"{nmod.name} error occured - skipping: {ex}")
                json_log_state("run_actions", "error", f"{ex}", mod=nmod.name)
                if Config.VERBOSE:
                    traceback.print_exc()
        json_log_state("run_actions", "complete")

    curr = 0
    finished = True
    for nmod in mods:
        curr += 1
        log_status(nmod.name, curr, maximum, "")
        for archive in nmod.download_info:
            c = cache.get_installed(nmod, archive)
            if c.get("downloaded") and c.get("extracted") and c.get("actions_done"):
                continue
            if Config.DOWNLOAD_ONLY and c.get("downloaded"):
                continue
            logger.info(f"\t  - {archive.file_name}")
            if c.get("downloaded"):
                logger.info("\t    - [x] downloaded")
            else:
                logger.info("\t    - [ ] downloaded")
                finished = False
            if c.get("extracted"):
                logger.info("\t    - [x] extracted")
            else:
                logger.info("\t    - [ ] extracted")
                if not Config.DOWNLOAD_ONLY:
                    finished = False
            if c.get("actions_done"):
                logger.info("\t    - [x] actions done")
            else:
                logger.info("\t    - [ ] actions done")
                if not Config.DOWNLOAD_ONLY:
                    finished = False

    if finished:
        if not Config.DOWNLOAD_ONLY:
            logger.info(f"setting up {Config.MODLIST} finished successfully")
        else:
            logger.info(f"downloading {Config.MODLIST} finished successfully")
    else:
        if not Config.DOWNLOAD_ONLY:
            logger.error(f"could not complete setting up all mods for {Config.MODLIST}")
            logger.error(f"check the summary above for more details")
        else:
            logger.error(f"could not download all mods for {Config.MODLIST}")
            logger.error(f"check the summary above for more details")

    if len(manual) > 0:
        logger.info("")
        logger.info("")
        logger.info("Download these mods manually:")
        for mod in manual:
            logger.info(
                mod.name + f": {Config.MODDING_OPENMW_HOST}/mods/" + mod.slug + "/"
            )

    cache.close()


# cache


cache_types = {
    "installed": cache.INSTALLED_STATE_ID,
    "nexus": cache.NEXUS_CACHE_ID,
    "momw": cache.MOD_CACHE_ID,
    "synced": cache.SYNC_ID,
}


def cache_patch(args):
    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    cache_id = cache_types[args.type]
    path = "$." + args.path

    if not args.json_path:
        value = json.loads(str(args.value))
        cache.set_data(cache_id, path, value)
    else:
        value = cache.get_data(cache_id, "$." + args.value)
        cache.set_data(cache_id, path, value)

    res = cache.get_data(cache_id, ".".join(path.split(".")[:-1]))
    sys.stdout.write(json.dumps(res, indent=2))
    sys.stdout.write("\n")
    cache.close()


def cache_query(args):
    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    cache_id = cache_types[args.type]

    path = "$"
    if args.path and not args.path == ".":
        path = "$." + args.path

    res = cache.get_data(cache_id, path)
    sys.stdout.write(json.dumps(res, indent=2))
    sys.stdout.write("\n")


def cache_print_content_links(args):

    Config.MODLIST = args.modlist
    cache.init()

    mods = []
    manual = []
    if cache.get_data(cache.MOD_CACHE_ID, f"$.{Config.MODLIST}"):
        mods = list(
            map(
                lambda x: ModDesc.validate(x),
                cache.get_data(cache.MOD_CACHE_ID, f"$.{Config.MODLIST}.mods"),
            )
        )
        manual = list(
            map(
                lambda x: ModDesc.validate(x),
                cache.get_data(cache.MOD_CACHE_ID, f"$.{Config.MODLIST}.manual"),
            )
        )
    else:
        sys.stderr.write(f"please run this first: umo cache sync {args.modlist}\n")
        sys.exit(2)

    if args.subset:
        mods = list(filter(lambda x: x.dir in args.subset.split(","), mods))
        manual = list(filter(lambda x: x.dir in args.subset.split(","), manual))
    if args.skip:
        mods = list(filter(lambda x: x.dir not in args.skip.split(","), mods))
        manual = list(filter(lambda x: x.dir not in args.skip.split(","), manual))

    if args.skip_tags:
        mods = list(
            filter(
                lambda x: not any([tag in args.skip_tags.split(",") for tag in x.tags]),
                mods,
            )
        )
        manual = list(
            filter(
                lambda x: not any([tag in args.skip_tags.split(",") for tag in x.tags]),
                manual,
            )
        )

    direct = []
    nexus = []

    size = 0
    for moddesc in mods:
        for info in moddesc.download_info:
            # check if the all mods are synced
            synced = cache.get_synced(moddesc, info)
            if synced.get("generic_data", -1) == -1:
                logger.debug("no generic_data for " + info.file_name)
                sys.stderr.write(
                    "cache not complete - please run this again: umo cache sync"
                    f" {args.modlist}\n"
                )
                sys.exit(2)

            # sum the size overall
            size += cache.synced_size(synced)

    for mod in mods:
        if mod.handler == "nexus":
            nexus.append(mod)
        else:
            direct.append(mod)

    import handlers
    sys.stdout.write("[")
    first = True
    for mod in nexus:
        if not first:
            sys.stdout.write(",")
        else:
            first = False
        handler = handlers.HANDLERS[mod.handler]()
        links = handler.download_links(mod, sync=True, nosync=True)
        j = list(map(lambda x: {"content_link": x.content_link, "file_name": x.file_name}, links))
        sys.stdout.write(json.dumps({"mod": mod.name, "archives": j}, indent=2))

    sys.stdout.write("]")
    print("")



def cache_sync(args):
    import traceback

    import tqdm

    import handlers
    import requests

    check(args, True)
    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    if args.momw_url:
        Config.MODDING_OPENMW_HOST = args.momw_url
    elif args.beta:
        Config.MODDING_OPENMW_HOST = "https://beta.modding-openmw.com"
    elif args.use_dev_mods:
        Config.USE_DEV_MODS = True
    if args.verbose:
        Config.VERBOSE = True
        log.set_level(logging.DEBUG)
    if args.threads:
        Config.MAX_THREADS = int(args.threads)
    clrs = colour_generator()

    print_nexus_info()

    import asyncfn  # load after config set

    Config.MODLIST = args.modlist
    Config.BASEPATH = f"{Config.BASEPATH}/{Config.MODLIST}"

    progress = None
    mods = []
    manual = []
    momw_lists = requests.get(Config.MODDING_OPENMW_HOST + "/lists/json").json()
    if args.modlist not in momw_lists or args.skip_momw:
        progress = tqdm.tqdm(disable=True)

        mods = list(
            map(
                lambda x: ModDesc.validate(x),
                cache.get_data(cache.MOD_CACHE_ID, f"$.{Config.MODLIST}.mods"),
            )
        )
    else:
        progress = tqdm.tqdm(
            desc="syncing momw data",
            disable=Config.JSON_OUTPUT,
            total=100,
            colour=next(clrs),
            bar_format="{l_bar}{bar}| {n_fmt}/{total_fmt}",
        )
        json_log_state("syncing_momw_data", "init", None)
        json_log_state("syncing_momw_data", "running", None, 10, 100)
        progress.update(10)
        progress.refresh()
        mods, manual = get_mods(Config.MODLIST)
        json_log_state("syncing_momw_data", "running", None, 20, 100)
        progress.update(10)
        progress.refresh()
        mod_data = list(map(lambda x: json.loads(x.model_dump_json()), mods))  # TODO
        manual_data = list(map(lambda x: json.loads(x.model_dump_json()), manual))
        json_log_state("syncing_momw_data", "running", None, 30, 100)
        progress.update(10)
        progress.refresh()
        cache.set_data(
            cache.MOD_CACHE_ID,
            f"$.{Config.MODLIST}",
            {"mods": mod_data, "manual": manual_data},
        )
        json_log_state("syncing_momw_data", "running", None, 70, 100)
        progress.update(40)
        progress.refresh()
        mods_by_name = {}
        for mod in mod_data:
            mods_by_name[mod["dir"]] = mod

        cache.set_data(
            cache.MOD_CACHE_ID, f"$.{Config.MODLIST}.mods_by_name", mods_by_name
        )

        json_log_state("syncing_momw_data", "running", None, 80, 100)
        progress.update(10)
        progress.refresh()

    if args.subset:
        mods = list(filter(lambda x: x.dir in args.subset.split(","), mods))
    if args.skip:
        mods = list(filter(lambda x: x.dir not in args.skip.split(","), mods))
    if args.skip_tags:
        mods = list(
            filter(
                lambda x: not any([tag in args.skip_tags.split(",") for tag in x.tags]),
                mods,
            )
        )

    progress.update(10)
    progress.refresh()
    json_log_state("syncing_momw_data", "running", None, 90, 100)

    cache.replace_data(cache.NEXUS_CACHE_ID, {})
    maximum = len(mods)
    curr = 0

    fns = []
    for x in mods:
        curr = curr + 1

        def gensyncfn(m, c):
            def fn():
                nonlocal m
                nonlocal c
                try:
                    handler = handlers.HANDLERS[m.handler]()
                    handler.sync(m)
                except Exception as ex:
                    logger.error(f"{c} - {m.name}:")
                    logger.error(f"- error received - skipping: {ex}")
                    json_log_state("syncing", "error", f"{ex}", None, None, m.name)
                    if Config.VERBOSE:
                        traceback.print_exc()

            return asyncfn.FnLogWrapper(fn, m.name)

        fns.append(gensyncfn(x, curr))
    progress.update(10)
    json_log_state("syncing_momw_data", "done", None, 100, 100)
    progress.refresh()
    progress.close()
    # we need some locking / retry for sqlite here
    asyncfn.run_async(
        fns, True, len(mods), "syncing mod  data", next(clrs), "syncing_mod_data"
    )
    cache.close()


def cache_reset(args):
    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    if args.type == "all":
        cache.replace_data(cache.INSTALLED_STATE_ID, {})
        cache.replace_data(cache.NEXUS_CACHE_ID, {})
        cache.replace_data(cache.MOD_CACHE_ID, {})
    else:
        cache.replace_data(cache_types[args.type], {})
    cache.close()


def cache_list(args):
    import functools

    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    res = cache.get_data(cache_types[args.type], "$")

    for list_name in res.keys():
        Config.MODLIST = list_name
        count = 0
        if args.type == "momw":
            mod_count = len(res[list_name]["mods"])
            manual_count = len(res[list_name]["manual"])
            all_count = mod_count + manual_count
            archive_count = 0
            size = 0
            for mod in res[list_name]["mods"]:
                archive_count += len(mod["download_info"])
                moddesc = ModDesc.model_validate(mod)
                for info in moddesc.download_info:
                    size += cache.synced_size(cache.get_synced(moddesc, info))

            size = sizeof_fmt(size)
            count = f"{all_count}/{archive_count}/{size}"
            curr = 0
            for nmod in res[list_name]["mods"]:
                curr += 1
                log_status(nmod["name"], curr, mod_count, "")

        elif args.type == "installed":
            mod_list = cache.get_data(cache_types["momw"], f"$.{list_name}.mods")
            mods = len(mod_list)
            archive_count = 0
            downloaded = 0
            actions_done = 0
            for m in mod_list:
                mod = ModDesc.model_validate(m)
                archive_count += len(mod.download_info)
                for i in mod.download_info:
                    installed = cache.get_installed(mod, i)
                    if installed.get("downloaded"):
                        downloaded += 1
                    if installed.get("actions_done"):
                        actions_done += 1

            count = f"{mods}/{archive_count}/{downloaded}/{actions_done}"

        sys.stdout.write("{:40s} {}\n".format(list_name, count))


def cache_dump(args):
    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    res = cache.get_data(cache.MOD_CACHE_ID, f"$.{args.modlist}")
    if res:
        sys.stdout.write(json.dumps(res["mods"] + res["manual"], indent=2))
        sys.stdout.write("\n")


def cache_add(args):
    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    Config.MODLIST_FILE = True
    mods, manual = get_mods(args.file)
    mod_data = list(map(lambda x: json.loads(x.model_dump_json()), mods))  # TODO
    manual_data = list(map(lambda x: json.loads(x.model_dump_json()), manual))
    list_name = args.list_name or os.path.basename(os.path.splitext(args.file)[0])
    cache.set_data(
        cache.MOD_CACHE_ID,
        f"$.{list_name}",
        {"mods": mod_data, "manual": manual_data},
    )
    mods_by_name = {}
    for mod in mod_data:
        mods_by_name[mod["dir"]] = mod

    cache.set_data(
        cache.MOD_CACHE_ID,
        f"$.{list_name}.mods_by_name",
        mods_by_name,
    )

    cache.close()


def cache_done(args):
    cache.init()

    modlist = args.mod_list
    mod = args.mod
    raw_mod_data = cache.get_data(cache.MOD_CACHE_ID, f"$.{modlist}.mods_by_name.{mod}")

    if not raw_mod_data:
        logger.error(f"there is no mod {mod} in list {modlist}")
        sys.exit(1)

    moddesc = ModDesc.model_validate(raw_mod_data)
    for archive in moddesc.download_info:
        c = cache.get_installed(moddesc, archive)
        cache.cache_installed(moddesc, archive, "extracted", True)
        cache.cache_installed(moddesc, archive, "downloaded", True)
        cache.cache_installed(moddesc, archive, "actions_done", True)
    cache.close()


def cache_repl(args):
    signal.signal(signal.SIGINT, signal_handler)
    cache.init()

    INSTALLED = cache.get_data(cache.INSTALLED_STATE_ID)
    SYNCED = cache.get_data(cache.SYNC_ID)
    MOMW = cache.get_data(cache.MOD_CACHE_ID)
    NEXUS = cache.get_data(cache.NEXUS_CACHE_ID)

    vars = globals()
    vars.update(locals())

    import code
    import readline
    import rlcompleter

    readline.set_completer(rlcompleter.Completer(vars).complete)
    readline.parse_and_bind("tab: complete")
    repl = code.InteractiveConsole(locals={**globals(), **locals()})
    repl.interact(banner="""--- UMO REPL ---
Bound vars: INSTALLED, SYNCED, MOMW, NEXUS

Have fun and break stuff!""")


def add(args):
    import datetime
    import re
    import shutil

    import handlers
    import requests

    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    modlist = "custom"
    if hasattr(args, "modlist") and args.modlist:
        modlist = args.modlist

    if not shutil.which(Config.BIN_7Z):
        logger.error("7z must be in PATH")
        sys.exit(1)

    dinfo = None
    mod = None

    if args.nxmlink.startswith("nxm://morrowind/mods"):
        matches = re.search(r"nxm://morrowind/mods/(.*?)/files/(.*?)\?.*", args.nxmlink)
        mod_id = matches.group(1)
        file_id = matches.group(2)
        nxm = umonxm.Nxm(Config.NEXUS_API_KEY)
        details = nxm.mod_details("morrowind", mod_id)
        file_details = nxm.mod_file_details("morrowind", mod_id, file_id)

        dinfo = DownloadInfo(
            direct_download=None,
            extract_to=sanitize(details["name"]) + "/" + sanitize(file_details["name"]),
            actions=[],
            file_name=file_details["name"],
            nexus_file_id=int(file_id),
        )

        mod = ModDesc(
            name=details["name"],
            category="umo",
            handler="nexus",
            nexus_id=mod_id,
            url=f"https://www.nexusmods.com/morrowind/mods/{mod_id}",
            author=details["author"],
            description=details["summary"],
            dl_url="",
            usage_notes=details["description"],
            dir=sanitize(details["name"]),
            date_added=datetime.datetime.now(),
            date_updated=datetime.datetime.now(),
            on_lists=[modlist],
            slug="no-momw-mod-no-slug",
            download_info=[dinfo],
            compat=1,
            status=1,
        )
    elif args.nxmlink.startswith("momw://"):
        url = args.nxmlink.replace("momw://", "http://")
        body = requests.get(url).json()
        mod = ModDesc.validate(body)
        dinfo = mod.download_info[0]
        for key, handler in handlers.HANDLERS.items():
            h = handler()
            if h.can(mod):
                mod.handler = key
                break
        if not dinfo.file_name:
            dinfo.file_name = mod.name
    else:
        logger.error("no momw or nxm link")
        sys.exit(1)

    existing_mods = cache.get_data(cache.MOD_CACHE_ID, f"$.{modlist}.mods")
    found = False
    if existing_mods:
        for m in existing_mods:
            if m["name"] == mod.name:
                found = True
                for i in m["download_info"]:
                    if i["file_name"] == dinfo.file_name:
                        logger.warning("mod file already exists")
                        send_notification("UMO", "Mod is already on custom list")
                        return False
                m["download_info"].append(json.loads(dinfo.model_dump_json()))
                cache.set_data(cache.MOD_CACHE_ID, f"$.{modlist}.mods", existing_mods)
                cache.set_data(
                    cache.MOD_CACHE_ID, f"$.{modlist}.mods_by_name.{m['dir']}", m
                )
                break

    if not found:
        cache.set_data(cache.MOD_CACHE_ID, f"$.{modlist}.mods[#]", mod)
        cache.set_data(cache.MOD_CACHE_ID, f"$.{modlist}.mods_by_name.{mod.dir}", mod)
        cache.set_data(cache.MOD_CACHE_ID, f"$.{modlist}.manual", [])

    cache.close()
    send_notification("UMO", "Added mod to custom list")
    cache.init()

    Config.MODLIST = modlist
    Config.BASEPATH = f"{Config.BASEPATH}/{modlist}"

    if not (mod.handler == "nexus" and args.nxmlink.startswith("momw://")):
        handler = handlers.HANDLERS[mod.handler]()
        if mod.handler == "nexus":
            handler.download_link = args.nxmlink
        handler.sync(mod)
    cache.close()
    return True


def remove(args):
    signal.signal(signal.SIGINT, signal_handler)
    cache.init()
    modlist = "custom"
    if hasattr(args, "modlist") and args.modlist:
        modlist = args.modlist

    mod = args.mod

    existing_mods = cache.get_data(cache.MOD_CACHE_ID, f"$.{modlist}.mods")
    found = False
    result_mods = list(filter(lambda x: x["dir"] != mod, existing_mods))

    if len(existing_mods) > len(result_mods):
        cache.set_data(cache.MOD_CACHE_ID, f"$.{modlist}.mods", result_mods)
        cache.set_data(cache.MOD_CACHE_ID, f"$.{modlist}.mods_by_name", {})
        for mod in result_mods:
            cache.set_data(
                cache.MOD_CACHE_ID,
                f"$.{modlist}.mods_by_name.{mod['dir']}",
                mod,
            )
        cache.close()
        return True
    else:
        logger.warning("mod doesn't exists in this list")
        return False


def info(args):
    print_nexus_info()
    print(f"version:\t\t{Config.APP_NAME}-{Config.APP_VER}")
    print("cache dir:\t\t" + Config.CACHE_DIR)
    print("config dir:\t\t" + config_dir())
    print("basepath dir:\t\t" + Config.BASEPATH)
    print("morrowind dir:\t\t" + Config.MORROWIND_DIR)
    print("tes3cmd executable:\t" + Config.TES3CMD)
    this_config_file = config_file()
    logger.warn(
        "Most of these paths can be updated by running 'umo reconfig' or by editing"
        f" the config at {this_config_file}"
    )


def check(args, silent=False):
    import shutil

    if os.environ.get("UMO_NO_CHECKS", False):
        return

    def print_silent(txt, no_new_line=False):
        if not silent:
            if no_new_line:
                sys.stdout.write(txt)
            else:
                print(txt)

    if not shutil.which(Config.TES3CMD) and not os.environ.get(
        "UMO_SKIP_TES3CMD_CHECK", False
    ):
        logger.error(f"{Config.TES3CMD} is either not existent or not executable")
        sys.exit(1)

    if not Path(Config.MORROWIND_DIR).is_dir() and not os.environ.get(
        "UMO_SKIP_MW_DIR_CHECK", False
    ):
        logger.error(f"{Config.MORROWIND_DIR} is not a directory")
        sys.exit(1)

    print_silent("checking PATH for extraction tools")

    sz_found = False
    rar_found = False
    if shutil.which("7z"):
        print_silent("7z:    FOUND")
        sz_found = True
        # TODO maybe try to extract a rar file for checking?
    elif shutil.which("7zz"):
        print_silent("7z:    FOUND")
        sz_found = True
        Config.BIN_7Z = "7zz"
    else:
        print_silent("7z:    MISSING")

    if shutil.which("unrar"):
        print_silent("unrar: FOUND")
        rar_found = True
    else:
        print_silent("unrar: MISSING")

    if shutil.which("unar"):
        print_silent("unar:  FOUND")
        rar_found = True
    else:
        print_silent("unar:  MISSING")

    if not sz_found and rar_found:
        logger.warning(
            "7z is recommended but not necessary as it speeds up the extraction process"
        )
    if not sz_found and not rar_found:
        logger.error(
            "you don't have any rar file support - "
            "please install at least unrar, unar or "
            "a recent 7z version with rar file support"
        )
        sys.exit(1)

    import rar

    if rar.run_rar_tests(silent):
        if not silent:
            logger.info("all is good there should be no problems with extracting")
    else:
        logger.error("there were problems during testing the extraction of rar files")
        sys.exit(1)


def setup(args):
    import platform
    import shutil
    import subprocess

    if platform.system() == "Darwin":
        logger.info("nothing to do on mac os")
    elif platform.system() == "Windows":
        import winreg

        umomwd = get_real_argv0().replace("python.exe", "pythonw.exe")
        path_adjustments = False

        if not checks.nxm_handler_set_up() and yes_or_no(
            "Do you want to set umo as the default handler for nxm links (umo won't"
            " work without this)?"
        ):
            logger.info("registering umo as default nxm link handler")

            with winreg.CreateKey(
                winreg.HKEY_CURRENT_USER, r"Software\Classes\nxm"
            ) as key:
                winreg.SetValue(key, "", winreg.REG_SZ, "URL:nxm Protocol")
                winreg.SetValueEx(key, "URL Protocol", 0, winreg.REG_SZ, "")

            with winreg.CreateKey(
                winreg.HKEY_CURRENT_USER, r"Software\Classes\momw"
            ) as key:
                winreg.SetValue(key, "", winreg.REG_SZ, "URL:momw Protocol")
                winreg.SetValueEx(key, "URL Protocol", 0, winreg.REG_SZ, "")

            with winreg.CreateKey(
                winreg.HKEY_CURRENT_USER, r"Software\Classes\nxm\shell\open"
            ) as key:
                winreg.SetValue(key, "command", winreg.REG_SZ, f'{umomwd} "%1"')

            with winreg.CreateKey(
                winreg.HKEY_CURRENT_USER, r"Software\Classes\momw\shell\open"
            ) as key:
                winreg.SetValue(key, "command", winreg.REG_SZ, f'{umomwd} "%1"')

        path_sevenzip = r"C:\Program Files\7-zip"
        if not shutil.which(Config.BIN_7Z):
            if not Path(path_sevenzip).exists():
                if shutil.which("winget"):
                    logger.info(
                        "Haven't found any 7-zip installation - but found winget."
                    )
                    if yes_or_no("Do you want to install 7-zip?"):
                        logger.info("Calling 'winget install 7zip.7zip'")
                        if os.system("winget install 7zip.7zip") == 0:
                            if not Path(path_sevenzip).exists():
                                if Path(r"C:\7-zip").exists():
                                    path_sevenzip = r"C:\7-zip"

                if not Path(path_sevenzip).exists():
                    while not path_sevenzip.endswith("7z.exe"):
                        path_sevenzip = input("Provide the path to 7z.exe: ")
                        logger.warning("that didn't work - try again")
                    path_sevenzip = os.path.dirname(path_sevenzip)

            if Path(path_sevenzip).exists():
                if yes_or_no(
                    f"Do you want to add {path_sevenzip} to your PATH variable?"
                ):
                    path_adjustments = True
                    with winreg.OpenKey(
                        winreg.HKEY_CURRENT_USER,
                        "Environment",
                        0,
                        winreg.KEY_ALL_ACCESS,
                    ) as key:
                        orig_path, _ = winreg.QueryValueEx(key, "Path")
                        if not path_sevenzip in str(orig_path):
                            updated_path = orig_path + os.pathsep + path_sevenzip
                            winreg.SetValueEx(
                                key, "Path", 0, winreg.REG_EXPAND_SZ, updated_path
                            )
                            os.environ["PATH"] = updated_path
                            sys.path.append(path_sevenzip)
                        else:
                            os.environ["PATH"] = orig_path
        if path_adjustments:
            logger.info(
                f"you may have to close and open the shell again for the path changes"
                f" to take effect"
            )
    else:
        d = os.path.expanduser("~/.local/share/applications/")
        Path(d).mkdir(parents=True, exist_ok=True)
        binary = get_real_argv0()
        icon = os.path.expanduser("~/.config/umomwd/umomwd.ico")
        print(f"creating destkop entry at {d}umo.desktop")
        with open(d + "umo.desktop", "wb") as f:
            f.write(f"""[Desktop Entry]
Type=Application
Name=umo NXM/MOMW Scheme Handler
Exec={binary} %u
StartupNotify=false
Icon={icon}
Terminal=false
NoDisplay=true
MimeType=x-scheme-handler/nxm;x-scheme-handler/momw;
""".encode("utf8"))
            f.flush()

        print("registering umo as default nxm link handler")
        proc = subprocess.run(
            ["xdg-mime", "default", "umo.desktop", "x-scheme-handler/nxm"],
            stdout=subprocess.DEVNULL,
        )

        if proc.returncode != 0:
            logger.error("that did not work :(")

        print("registering umo as default momw link handler")
        proc = subprocess.run(
            ["xdg-mime", "default", "umo.desktop", "x-scheme-handler/momw"],
            stdout=subprocess.DEVNULL,
        )

        if proc.returncode != 0:
            logger.error("that did not work :(")

        print("running update-desktop-database")
        proc = subprocess.run(
            [
                "update-desktop-database",
                os.path.expanduser("~/.local/share/applications"),
            ],
            stdout=subprocess.DEVNULL,
        )

        if proc.returncode != 0:
            logger.error("that did not work :(")

    check(args, True)

    print("""You're now ready to install a mod list! Example:
umo cache sync i-heart-vanilla
umo install i-heart-vanilla""")
