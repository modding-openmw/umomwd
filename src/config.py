# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os

from platformdirs import user_cache_dir, user_config_dir


def config_dir():
    return os.environ.get("UMO_CONF_DIR", user_config_dir("umomwd"))


def config_file():
    return os.environ.get("UMO_CONF_FILE", os.path.join(config_dir(), "config.json"))


# TODOOOO
class Config:
    # headers for nexusmods (see
    # https://help.nexusmods.com/article/114-api-acceptable-use-policy)
    APP_VER_HEADER = "Application-Version"
    APP_VER = "0.8.18"
    APP_NAME_HEADER = "Application-Name"
    APP_NAME = "umo"
    USER_AGENT = f"{APP_NAME}/{APP_VER}"

    CURL_HEADER = [
        f"{APP_VER_HEADER}: {APP_VER}",
        f"{APP_NAME_HEADER}: {APP_NAME}",
        f"User-Agent: {USER_AGENT}",
    ]
    REQ_HEADER = {
        APP_VER_HEADER: APP_VER,
        APP_NAME_HEADER: APP_NAME,
        "User-Agent": USER_AGENT,
    }

    MORROWIND_DIR = "."
    TES3CMD = "tes3cmd"
    SOCKET = ("127.0.0.1", 6666)
    SOCKET_SERVER = None
    MODDING_OPENMW_HOST = os.environ.get(
        "MODDING_OPENMW_HOST", "https://modding-openmw.com"
    )

    NEXUS_API_KEY = None
    BASEPATH = "./OpenMWMods"
    CACHE_DIR = user_cache_dir("umomwd")

    VERBOSE = False
    NO_GUI = False
    NO_NOTIFICATIONS = bool(os.environ.get("UMO_NO_NOTIFICATIONS", False))
    NEXUS_PREMIUM = False
    DOWNLOAD_ONLY = False
    MODLIST_FILE = False
    MODLIST = None
    MAX_THREADS = 10
    JSON_OUTPUT = bool(os.environ.get("YAMZ", False))
    USE_DEV_MODS = False
    BIN_7Z = "7z"
    MAX_SPEED = False
