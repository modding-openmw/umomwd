# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import getpass
import json
import sys
import uuid

import pwinput
from websockets.sync.client import connect

import helper
import log

logger = log.get_logger()


def get_nexus_key():
    req_id = str(uuid.uuid4())
    try:
        print("connecting to sso.nexusmods.com...")
        with connect("wss://sso.nexusmods.com") as websocket:
            payload = {}
            payload["id"] = req_id
            payload["appid"] = "umo"
            websocket.send(json.dumps(payload))
            url = f"https://www.nexusmods.com/sso?id={req_id}&application=umo"
            helper.open_url(url)
            print(f"open {url} if it didn't open automatically")
            message = websocket.recv()
            return message
    except Exception as ex:
        logger.error("sso authentication failed")
        nexus_key = pwinput.pwinput(
            prompt="""Open this link in your browser, you will need to log in:
https://next.nexusmods.com/settings/api-keys
Click the "Request API Key" button for umo.
Paste this key here (please note that for security reasons nothing will be shown here when you paste):"""
        )
        nexus_key = "".join(s for s in nexus_key if s.isprintable())
        while not nexus_key:
            nexus_key = getpass.getpass(
                """ERROR: Windows paste problem detected (or an empty key was pasted)!
Please retry pasting your key but please **do it with a right click**:"""
            )
            nexus_key = "".join(s for s in nexus_key if s.isprintable())

        return nexus_key
