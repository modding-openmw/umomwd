# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import os
from pathlib import Path

from config import Config, config_dir

if not Config.NO_NOTIFICATIONS:
    from desktop_notifier import DesktopNotifier
    from desktop_notifier.common import Icon


def send_notification(title, text):
    if not Config.NO_GUI and not Config.NO_NOTIFICATIONS:
        notifier = DesktopNotifier(
            app_name="umo",
            app_icon=Icon(Path(os.path.join(config_dir(), "openmw.ico"))),
        )

        async def doit():
            allowed = await notifier.has_authorisation()
            if allowed:
                await notifier.send(title, text, timeout=3)
            else:
                Config.NO_NOTIFICATIONS = True

        asyncio.run(doit())
