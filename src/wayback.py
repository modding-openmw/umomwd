# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re

from dl import curl_get_body


def get_archived_nexus_file_ids(mod_id):
    ids = []
    search_url = f"https://www.nexusmods.com/morrowind/mods/{mod_id}?tab=files"
    versions_response = curl_get_body(
        f"http://web.archive.org/cdx/search/cdx?url={search_url}"
    )
    if len(versions_response) > 0:
        m = re.search(r"[0-9]{14}", versions_response)
        if m:
            cache_date = m.group(0)
            archived_page = curl_get_body(
                f"https://web.archive.org/web/{cache_date}/{search_url}"
            )
            r = list(
                map(
                    lambda x: x[1],
                    re.findall(r"href=\"(.*?file_id=([0-9]+).*?)\"", archived_page),
                )
            )

            return r
    return None
