# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import os
import shutil
import sqlite3
import threading
from pathlib import Path

import helper
from config import Config
from log import get_logger, set_level

MOD_CACHE_ID = 1
NEXUS_CACHE_ID = 3
INSTALLED_STATE_ID = 5
SYNC_ID = 6
NEXUS_URL_ID = 7

logger = get_logger()


def to_json(data, exclude=None):
    if hasattr(data, "model_dump_json"):
        return data.model_dump_json(exclude=exclude)
    return json.dumps(data)


con = None
con_real = None
lock = threading.Lock()


def close():
    if con:
        try:
            con.execute("COMMIT")
        except Exception as ex:
            logger.debug(ex)
        con_real.close()
        db_file = os.path.join(Config.CACHE_DIR, "cache.db")
        db_file_new = db_file + ".new"
        Path(db_file_new).unlink(missing_ok=True)
        logger.warn("persisting db")
        con.execute("VACUUM INTO ?", [f"{db_file}.new"])
        shutil.move(db_file + ".new", db_file)


def init():
    global con
    global con_real
    Path(Config.CACHE_DIR).mkdir(parents=True, exist_ok=True)
    db_file = os.path.join(Config.CACHE_DIR, "cache.db")
    con_real = sqlite3.connect(db_file, timeout=0.5)
    con = sqlite3.connect(":memory:", timeout=60.0, check_same_thread=False)
    con_real.execute("PRAGMA locking_mode = EXCLUSIVE")
    con_real.execute("BEGIN EXCLUSIVE")

    c = con_real.cursor()
    c.execute("""
    CREATE TABLE IF NOT EXISTS cache (
       id       INTEGER   PRIMARY KEY,
       data     JSON      DEFAULT '{}',
       modified DATETIME  DEFAULT CURRENT_TIMESTAMP,
       created  DATETIME  DEFAULT CURRENT_TIMESTAMP
    )
    """)

    # stuff to load existing old caches
    if Path("./mod_details_cache.json").exists():
        with open("./mod_details_cache.json", "rb") as f:
            c.execute(
                "INSERT INTO cache (id, data) VALUES (?, JSON(?))",
                (MOD_CACHE_ID, json.dumps(json.load(f))),
            )
            con.commit()
        os.unlink("./mod_details_cache.json")
    if Path("./nexus_cache.json").exists():
        with open("./nexus_cache.json", "rb") as f:
            c.execute(
                "INSERT INTO cache (id, data) VALUES (?, JSON(?))",
                (NEXUS_CACHE_ID, json.dumps(json.load(f))),
            )
        os.unlink("./nexus_cache.json")

    c.execute(
        "INSERT INTO cache (id) VALUES "
        f"({MOD_CACHE_ID}),"
        f"({NEXUS_CACHE_ID}),"
        f"({INSTALLED_STATE_ID}), "
        f"({SYNC_ID}), "
        f"({NEXUS_URL_ID}) "
        "ON CONFLICT (id) DO NOTHING"
    )

    con_real.commit()
    c.close()
    con_real.close()

    with open(db_file, "rb") as f:
        con.deserialize(f.read())

    # block this
    con_real = sqlite3.connect(db_file, timeout=0.5)
    con_real.execute("PRAGMA locking_mode = EXCLUSIVE")
    con_real.execute("BEGIN EXCLUSIVE")


def replace_data(id, data):
    logger.debug(f"replace_data {id}, {data}")
    global con
    j = to_json(data)
    with lock:
        c = con.cursor()
        query = (
            "INSERT INTO cache (id, data, modified) "
            "VALUES (?, JSON(?), datetime('now')) "
            "ON CONFLICT (id) DO "
            "UPDATE SET data = JSON(?), "
            "modified = datetime('now') WHERE id = ?"
        )
        c.execute(query, (id, j, j, id))
        con.commit()
        c.close()


def set_data(id, path, value):
    logger.debug(f"set_data {id}, {path}")
    j = to_json(value)
    with lock:
        c = con.cursor()
        c.execute(
            "UPDATE cache SET data = json_set(data, ?, JSON(?)), modified ="
            " datetime('now') WHERE id = ?",
            (path, j, id),
        )
        con.commit()
        c.close()


def get_data(id, path="$"):
    logger.debug(f"get_data {id}, {path}")
    with lock:
        c = con.cursor()
        res = c.execute(
            "SELECT json_extract(data, ?) FROM cache WHERE id = ?", (path, id)
        ).fetchone()

        con.commit()
        c.close()
        if res and res[0]:
            try:
                return json.loads(res[0])
            except Exception:
                # this might happen when the value is a raw value
                # we don't get properly encoded json back
                return res[0]

    return None


# optimize the synced cache
# this is okay here as we only write to it in cache sync
# and only read it in install
# IT'S NOT THREADSAFE TO READ + WRITE AT THE SAME TIME
synced_mem_cache = None
installed_mem_cache = None


def cache_installed(mod, data, subpath, value):
    global installed_mem_cache
    installed_mem_cache = None

    mod_name = helper.sanitize(mod.name)
    archive_name = helper.sanitize(data.file_name)
    set_data(
        INSTALLED_STATE_ID,
        f"$.{Config.MODLIST}.{mod_name}.{archive_name}.{subpath}",
        value,
    )


def get_installed(mod, data, fresh=True):
    # global installed_mem_cache

    # if not installed_mem_cache:
    #     installed_mem_cache = get_data(INSTALLED_STATE_ID, f"$.{Config.MODLIST}")

    mod_name = helper.sanitize(mod.name)
    archive_name = helper.sanitize(data.file_name)

    if fresh or True:
        return (
            get_data(
                INSTALLED_STATE_ID, f"$.{Config.MODLIST}.{mod_name}.{archive_name}"
            )
            or {}
        )
    else:
        return installed_mem_cache.get(mod_name, {}).get(archive_name, {})


def cache_synced(mod, data, subpath, value):
    global synced_mem_cache
    synced_mem_cache = None

    mod_name = helper.sanitize(mod.name)
    archive_name = helper.sanitize(data.file_name)
    set_data(
        SYNC_ID,
        f"$.{Config.MODLIST}.{mod_name}.{archive_name}.{subpath}",
        value,
    )


def get_synced(mod, data, fresh=False):
    global synced_mem_cache

    if not synced_mem_cache:
        synced_mem_cache = get_data(SYNC_ID, f"$.{Config.MODLIST}")

    mod_name = helper.sanitize(mod.name)
    archive_name = helper.sanitize(data.file_name)

    if fresh:
        return get_data(SYNC_ID, f"$.{Config.MODLIST}.{mod_name}.{archive_name}") or {}
    else:
        return synced_mem_cache.get(mod_name, {}).get(archive_name, {})


def generic_data(
    version, size=None, size_kb=None, hash_digest=None, hash_algo=None, url=None
):
    return {
        "version": version,
        "size_in_bytes": size,
        "size_kb": size_kb if size_kb else round(size / 1024) if size else None,
        "hash": {"type": hash_algo, "value": hash_digest},
        "url": url,
    }


def cache_nexus_dl_url(mod, data, url):
    mod_name = helper.sanitize(mod.name)
    archive_name = helper.sanitize(data.file_name)
    synced = get_synced(mod, data)
    version = synced.get("generic_data", {}).get("version") or "0"
    version = helper.sanitize(version)
    set_data(
        NEXUS_URL_ID,
        f"$.{Config.MODLIST}.{mod_name}.{archive_name}.{data.nexus_file_id}.{version}.url",
        url,
    )


def get_nexus_dl_url(mod, data):
    mod_name = helper.sanitize(mod.name)
    archive_name = helper.sanitize(data.file_name)
    synced = get_synced(mod, data)
    version = synced.get("generic_data", {}).get("version") or "0"
    version = helper.sanitize(version)
    return (
        get_data(
            NEXUS_URL_ID,
            f"$.{Config.MODLIST}.{mod_name}.{archive_name}.{data.nexus_file_id}.{version}.url",
        )
        or None
    )


def synced_size(synced):
    return (
        synced.get("generic_data", {}).get("size_in_bytes", 0)
        or (synced.get("generic_data", {}).get("size_kb", 0) or 0) * 1024
    )
