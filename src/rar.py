# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import base64
import os
import subprocess
import sys

from config import Config

rar_v5 = base64.b64decode(
    "UmFyIRoHAM+QcwAADQAAAAAAAAAGl3QgkC4AvAAAALYjAAACDlN2l6m5ZFkdMwsAIAAAAGdhcmJhZ2UudHh0AJB0DVTMzP4NALlJAJwSALG73jtAnR5lNXhKLy+gJl6xtyMS8BhwODyOC+/4f44kom40kUTwsj5A81zNT3j7Dz8120baGfvL1cvYPrxwY11RYm+jQMGKPs0DhjHC811RYm+lf/oro++mvfg6vhV+GggMbvC9eL0L15PSvX2lcroY1hN+/9+O9B1fCr8NBAY4eDQfBjl+mgkMUeV6/16l6/+9dr/igSr4WeTQcDFXpev29a9fx7F6+JcK8kE8PyDEPXsAQAcA"
)
rar_v4 = base64.b64decode(
    "UmFyIRoHAM+QcwAADQAAAAAAAAAky3QgkC4AtQAAALYjAAACDlN2l6m5ZFkdMwsAIAAAAGdhcmJhZ2UudHh0AJB04w7/H+gUYK4ZBWlR/jiYpVrk31w6TseUG4j9rF5/Mbvt31dQ88UO/q8FAi0kQYsyUJx3i+IFcHw5NOUFA0X4TD338ocXZU2HYXaKqchxLhYswhW1Gf/BMay4Z6sMHCtx5Lh5egvjkDbbZwbBrf1t9QCCT5ozl/4Di/dZNF0KgEC7DSp+6Jx3VFBzoukI/nP4xHU+Cf/rAN5rbiGSlH6ck+K7FfvuLcIdrgiibQAAv4hn9qn/1MQ9ewBABwA="
)

rar_v4_solid = base64.b64decode(
    "UmFyIRoHADvQcwgADQAAAAAAAABHRXTAkC4AvAAAALYjAAACDlN2l6m5ZFkdMwsAIAAAAGdhcmJhZ2UudHh0AJB0DVTMzP4NALlJAJwSALG73jtAnR5lNXhKLy+gJl6xtyMS8BhwODyOC+/4f44kom40kUTwsj5A81zNT3j7Dz8120baGfvL1cvYPrxwY11RYm+jQMGKPs0DhjHC811RYm+lf/oro++mvfg6vhV+GggMbvC9eL0L15PSvX2lcroY1hN+/9+O9B1fCr8NBAY4eDQfBjl+mgkMUeV6/16l6/+9dr/igSr4WeTQcDFXpev29a9fx7F6+JcK8kE8PyD9jXTQkCsAJAAAAAgAAAACniX1ryC+XFkdMwgAIAAAAHRlc3QudHh0ANBJDM0Qz+TPJpHMleBP9/EB/FvdHXKk4AsXQCtOHE01vlee6+/AQ9x00JAuACAAAAC2IwAAAg5TdpcQBXhZHTMLACAAAABnYXJiYWdlLmJpbgCQRu0OXhO52AtueKXhO52AvGaKXhO52AvXUHLwnc7AXX9AxD17AEAHAA=="
)


class TestRarFile:
    def __init__(self, testname, content, files):
        self.testname = testname
        self.content = content
        self.files = files
        self.result = False


test_rar_files = [
    TestRarFile("RARv4", rar_v4, ["garbage.txt"]),
    TestRarFile("RARv5", rar_v5, ["garbage.txt"]),
    TestRarFile(
        "RARv4 solid", rar_v4_solid, ["garbage.txt", "garbage.bin", "test.txt"]
    ),
]


def run_rar_tests(silent):
    def print_silent(txt, no_new_line=False):
        if not silent:
            if no_new_line:
                sys.stdout.write(txt)
            else:
                print(txt)

    from dl import extract_python

    for t in test_rar_files:
        ok = False
        with open(".tmp.rar", "wb") as f:
            f.write(t.content)
        print_silent(f"{t.testname} python: ", True)
        try:
            extract_python(".tmp.rar", ".")
        except:
            ok = False

        if all([os.path.exists(x) for x in t.files]):
            print_silent("\t\033[32;1;1mOK\033[0m\n", True)
            ok = True
        else:
            print_silent("\t\033[31;1;1mNOT OK\033[0m\n", True)

        for f in t.files:
            if os.path.exists(f):
                os.unlink(f)

        print_silent(f"{t.testname} 7z: ", True)
        cmd = [Config.BIN_7Z, "x", "-bso0", "-bse2", "-bsp2", "-y", f"-o.", ".tmp.rar"]
        proc = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

        if proc.returncode == 0 and all([os.path.exists(x) for x in t.files]):
            print_silent("\t\033[32;1;1mOK\033[0m\n", True)
            ok = True
        else:
            print_silent("\t\033[31;1;1mNOT OK\033[0m\n", True)

        for f in t.files:
            if os.path.exists(f):
                os.unlink(f)

        os.unlink(".tmp.rar")
        t.result = ok

    return all(map(lambda x: x.result, test_rar_files))
