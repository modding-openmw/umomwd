# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import os
import shutil
import stat
import subprocess
import sys
import traceback
from datetime import datetime
from pathlib import Path
from typing import List, Literal, Optional, Union

from pydantic import BaseModel

from log import get_logger

logger = get_logger()


# taken from https://stackoverflow.com/questions/2656322/shutil-rmtree-fails-on-windows-with-access-is-denied
# to maybe fix broken win permissions
def rmtree_onerror(func, path, exc_info):
    """
    Error handler for ``shutil.rmtree``.

    If the error is due to an access error (read only file)
    it attempts to add write permission and then retries.

    If the error is for another reason it re-raises the error.

    Usage : ``shutil.rmtree(path, onerror=onerror)``
    """
    # Is the error an access error?
    if not os.access(path, os.W_OK):
        os.chmod(path, stat.S_IWUSR)
        func(path)
    else:
        raise


def yes_or_no(text):
    yes_choices = ["yes", "y"]
    no_choices = ["no", "n"]
    user_input = input(f"{text} (y/n): ")

    if user_input.lower() in yes_choices:
        return True
    elif user_input.lower() in no_choices:
        return False
    else:
        sys.stderr.write("type yes or no\n")
        return yes_or_no(text)


def check_path(path):
    cwd = os.getcwd()
    if not str(Path(path).absolute()).startswith(cwd):
        raise Exception(f"unallowed path in actions: {path}")


class BaseAction(BaseModel):
    action: str
    # TODO
    mw_dir: Optional[str] | None = None
    tes3cmd: Optional[str] | None = None

    def handle(self, basedir: str):
        pass

    def __str__(self):
        return self.action

    def get_paths(self):
        pass


class RemoveAction(BaseAction):
    action: Literal["remove"]
    path: Optional[str] | None = None
    paths: Optional[List[str]] | None = None

    def handle_one(self, path, basedir: str):
        try:
            check_path(path)
            logger.info(f"removing {path}")
            p = Path(basedir + path)
            if p.is_dir():
                shutil.rmtree(basedir + path, onerror=rmtree_onerror)
            else:
                os.unlink(basedir + path)
        except Exception as ex:
            logger.error(f"error on remove action: {ex}")
            traceback.print_exc()
            raise ex

    def handle(self, basedir: str):
        if self.path:
            self.handle_one(self.path, basedir)
        else:
            for p in self.paths:
                self.handle_one(p, basedir)

    def __str__(self):
        return f"{self.action}: {self.path}"

    def get_paths(self):
        if self.paths:
            return self.paths
        else:
            return [self.path]


class RenameAction(BaseAction):
    action: Literal["rename"]
    src: str
    dst: str

    def handle(self, basedir: str):
        try:
            check_path(self.src)
            check_path(self.dst)
            d = "/".join(self.dst.split("/")[:-1])
            Path(basedir + d).mkdir(parents=True, exist_ok=True)
            logger.info(f"renaming {self.src} to {self.dst}")
            shutil.move(basedir + self.src, basedir + self.dst)
        except Exception as ex:
            logger.error(f"error on rename action: {ex}")
            traceback.print_exc()
            raise ex

    def __str__(self):
        return f"{self.action}: {self.src} -> {self.dst}"

    def get_paths(self):
        return [self.src, self.dst]


class CopyAction(BaseAction):
    action: Literal["copy"]
    src: str
    dst: str
    force: Optional[bool] | None = None

    def handle(self, basedir: str):
        try:
            check_path(self.src)
            check_path(self.dst)
            d = "/".join(self.dst.split("/")[:-1])
            Path(basedir + d).mkdir(parents=True, exist_ok=True)
            logger.info(f"copying {self.src} to {self.dst}")
            if Path(basedir + self.src).is_dir():
                shutil.copytree(
                    basedir + self.src, basedir + self.dst, dirs_exist_ok=self.force
                )
            else:
                shutil.copy(basedir + self.src, basedir + self.dst)
        except Exception as ex:
            logger.error(f"error on copy action: {ex}")
            traceback.print_exc()
            raise ex

    def __str__(self):
        return f"{self.action}: {self.src} -> {self.dst}"

    def get_paths(self):
        return [self.src, self.dst]


class CleanAction(BaseAction):
    action: Literal["clean"]
    path: str
    arguments: Optional[str] | Optional[List[str]] | None = None

    def handle_broken_arguments(self):
        if self.arguments:
            if type(self.arguments) is str:
                return json.loads(self.arguments.replace("'", '"'))
            else:
                return self.arguments
        else:
            return []

    def handle(self, basedir: str):
        try:
            if not self.mw_dir:
                raise Exception("clean action triggered without mw_dir set")

            if not self.tes3cmd:
                raise Exception("clean action triggered without tes3cmd set")

            args = ["clean"]
            plugin_name = self.path.split("/")[-1]
            check_path(self.path)

            if self.arguments:
                # TODO build a json fuckup in momw where arguments are str
                # and not list of str
                args = self.handle_broken_arguments()

            res = plugin_name
            if args[0] == "clean":
                res = f"Clean_{plugin_name}"

            logger.info(f"cleaning {self.path} with args {args}")
            cwd = os.getcwd()
            plugin_name = self.path.split("/")[-1]
            plugin_dir = "/".join(self.path.split("/")[:-1]) + "/"
            shutil.copy(basedir + self.path, f"{self.mw_dir}/Data Files/{plugin_name}")
            os.chdir(f"{self.mw_dir}/Data Files/")
            cmd = [self.tes3cmd] + args + [plugin_name]
            env = os.environ.copy()
            env["PWD"] = os.getcwd()  # perl is PWD dependant
            subprocess.run(cmd, env=env)  # and we really don't want to
            # run this with a shell
            os.chdir(cwd)

            shutil.copy(
                f"{self.mw_dir}/Data Files/{res}", f"{basedir}{plugin_dir}{res}"
            )

            os.unlink(f"{self.mw_dir}/Data Files/{plugin_name}")
            if plugin_name != res:
                os.unlink(f"{self.mw_dir}/Data Files/{res}")
        except Exception as ex:
            logger.error(f"error on clean action: {ex}")
            traceback.print_exc()
            raise ex

    def __str__(self):
        arguments = " ".join(self.handle_broken_arguments())
        return f"{self.action}: {self.tes3cmd} {arguments} {self.path}"

    def get_paths(self):
        return [self.path]


class DownloadInfo(BaseModel):
    direct_download: str | None
    file_name: str | None
    extract_to: str | None
    actions: List[Union[RemoveAction, CleanAction, CopyAction, RenameAction]]
    nexus_file_id: Optional[int] | None = None
    # internal
    content_link: str | None = None



class ModDesc(BaseModel):
    name: str
    author: str
    description: str
    url: str
    category: str
    dl_url: str
    usage_notes: str | None
    compat: int
    dir: str
    slug: str
    tags: Optional[List[str]] = []
    date_added: datetime
    date_updated: datetime
    download_info: List[DownloadInfo]
    on_lists: List[str]
    # that's just for umomwd and not part of the MOMW json data
    handler: Optional[str] | None = None
    nexus_id: Optional[str] | None = None
    prepared: Optional[bool] | None = None
    conflicts: Optional[List[str]] | None = []

    def set_single_dl_link(self, link):
        if len(self.download_info) > 1:
            logger.warn("set single download link on multi archive mod")
            self.download_info = [self.download_info[0]]
            self.download_info[0].file_name = self.name
            self.download_info[0].extract_to = self.dir
            # raise Exception("Tried to set single download link on multi archive mod")

        self.download_info[0].direct_download = link
