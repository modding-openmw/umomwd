# Copyright (C) 2024 Sebastian Fieber <fallchildren@disroot.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import logging
import os
import sys

import coloredlogs

import helper
from config import Config

# silence curldl
logging.getLogger("curldl").setLevel(logging.WARNING)
logging.getLogger("umocurldl").setLevel(logging.WARNING)


LOG_LEVEL_STYLES = {
    "critical": {"bold": True, "color": "red"},
    "debug": {"color": "white"},
    "error": {"color": "red"},
    "info": {"color": "green"},
    "warning": {"color": "magenta"},
}


# TODO
def get_logger():
    loglevel = logging.INFO
    if Config.VERBOSE:
        loglevel = logging.DEBUG
    if Config.JSON_OUTPUT:
        loglevel = 1000  # disable logging completely

    logging.basicConfig(level=loglevel)
    coloredlogs.install(
        level=loglevel, fmt="%(message)s", level_styles=LOG_LEVEL_STYLES
    )

    return logging.getLogger()


def set_level(loglevel):
    coloredlogs.set_level(loglevel)


def _print(*args):
    if not Config.JSON_OUTPUT:
        sys.stderr.write(*args)


def json_log_state(
    action, state, payload=None, n=None, total=None, mod=None, archive=None
):
    if Config.JSON_OUTPUT:
        d = {
            "action": helper.slugify(action),
            "state": state,
            "payload": payload,
            "mod": mod,
            "archive": archive,
            "n": n,
            "total": total,
        }
        sys.stdout.write(
            json.dumps({k: v for k, v in d.items() if v is not None}) + "\n"
        )
        sys.stdout.flush()
