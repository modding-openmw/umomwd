# slimmed down variant of https://github.com/odwyersoftware/mega.py
# original license: Apache-2.0 license

import base64
import codecs
import json
import os
import shutil
import struct
import sys
import tempfile
from pathlib import Path

import tqdm
from Cryptodome.Cipher import AES
from Cryptodome.Util import Counter

import requests
from config import Config


def a32_to_str(a):
    return struct.pack(">%dI" % len(a), *a)


def str_to_a32(b):
    if isinstance(b, str):
        b = codecs.latin_1_encode(b)[0]
    if len(b) % 4:
        # pad to multiple of 4
        b += b"\0" * (4 - len(b) % 4)
    return struct.unpack(">%dI" % (len(b) / 4), b)


def base64_url_decode(data):
    data += "=="[(2 - len(data) * 3) % 4 :]
    for search, replace in (("-", "+"), ("_", "/"), (",", "")):
        data = data.replace(search, replace)
    return base64.b64decode(data)


def base64_to_a32(s):
    return str_to_a32(base64_url_decode(s))


def get_chunks(size):
    p = 0
    s = 0x20000
    while p + s < size:
        yield (p, s)
        p += s
        if s < 0x100000:
            s += 0x20000
    yield (p, size - p)


def get_size(mega_url):
    file_handle = mega_url.split("!")[1]
    url = "https://g.api.mega.co.nz/cs?id=0&v=2"
    j = {"a": "g", "ad": 1, "p": file_handle}
    r = requests.post(url, data=json.dumps([j]))
    return r.json()[0]["s"]


def download_mega(mega_url, dest, dl_fun):
    file_handle = mega_url.split("!")[1]
    file_key = mega_url.split("!")[2]

    file_key = base64_to_a32(file_key)
    k = (
        file_key[0] ^ file_key[4],
        file_key[1] ^ file_key[5],
        file_key[2] ^ file_key[6],
        file_key[3] ^ file_key[7],
    )
    iv = file_key[4:6] + (0, 0)
    meta_mac = file_key[6:8]

    url = "https://g.api.mega.co.nz/cs"
    j = {"a": "g", "g": 1, "p": file_handle}

    r = requests.post(url, data=json.dumps([j]))
    data = r.json()[0]

    tmpname = "." + os.path.basename(dest)
    dl_fun(data["g"], Path(tmpname).absolute())

    file_size = data["s"]

    dest_name = None
    with tempfile.NamedTemporaryFile(
        mode="w+b", prefix="megapy_", delete=False
    ) as temp_output_file:
        k_str = a32_to_str(k)
        counter = Counter.new(128, initial_value=((iv[0] << 32) + iv[1]) << 64)
        aes = AES.new(k_str, AES.MODE_CTR, counter=counter)

        mac_str = "\0" * 16
        mac_encryptor = AES.new(k_str, AES.MODE_CBC, mac_str.encode("utf8"))
        iv_str = a32_to_str([iv[0], iv[1], iv[0], iv[1]])

        with open(tmpname, "rb") as input_file:
            for chunk_start, chunk_size in tqdm.tqdm(
                get_chunks(file_size),
                total=int(file_size / 0x100000) + 4,
                desc=f"{tmpname}",
                colour="#669EB9",
                leave=False,
                disable=Config.JSON_OUTPUT,
            ):
                chunk = input_file.read(chunk_size)
                chunk = aes.decrypt(chunk)
                temp_output_file.write(chunk)

                encryptor = AES.new(k_str, AES.MODE_CBC, iv_str)
                for i in range(0, len(chunk) - 16, 16):
                    block = chunk[i : i + 16]
                    encryptor.encrypt(block)

                # fix for files under 16 bytes failing
                if file_size > 16:
                    i += 16
                else:
                    i = 0

                block = chunk[i : i + 16]
                if len(block) % 16:
                    block += b"\0" * (16 - (len(block) % 16))
                mac_str = mac_encryptor.encrypt(encryptor.encrypt(block))
        dest_name = temp_output_file.name
    os.unlink(tmpname)
    file_mac = str_to_a32(mac_str)
    # check mac integrity
    if (file_mac[0] ^ file_mac[1], file_mac[2] ^ file_mac[3]) != meta_mac:
        raise ValueError("Mismatched mac")
    output_path = Path(dest)
    shutil.move(dest_name, output_path)
