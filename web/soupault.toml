[settings]
  soupault_version = "4.3.1"
  strict = true
  # verbose = true
  # debug = true
  site_dir = "site"
  build_dir = "build"
  page_file_extensions = ["htm", "html", "md", "rst", "adoc"]
  ignore_extensions = ["draft", "kra"]
  generator_mode = true
  complete_page_selector = "html"
  default_template_file = "templates/main.html"
  default_content_selector = "div#content"
  default_content_action = "append_child"
  keep_doctype = true
  doctype = "<!DOCTYPE html>"
  pretty_print_html = true
  clean_urls = true
  plugin_discovery = true
  plugin_dirs = ["plugins"]

[preprocessors]
  md = "cmark --unsafe --smart"

[widgets.page-title]
  widget = "title"
  selector = "h1"
  default = "umo &mdash; An automatic modlist downloader for Modding-OpenMW.com"
  append = " | umo &mdash; An automatic modlist downloader for Modding-OpenMW.com"
  force = false

[widgets.generator-meta]
  widget = "insert_html"
  html = '<meta name="generator" content="soupault">'
  selector = "head"

[widgets.changelog-toc-html]
  widget = "insert_html"
  html = '<h3>Releases</h3><div id="toc"></div>'
  page = ["changelog.md"]
  selector = "h4"
  action = "insert_before"

[widgets.other-toc-html]
  widget = "insert_html"
  html = '<div id="toc"></div>'
  page = ["index.html"]
  selector = "h2"
  action = "insert_before"

[widgets.toc-heading]
  widget = "insert_html"
  html = '<h1>Table of contents</h1>'
  page = ["index.html"]
  selector = "div#toc"
  action = "insert_before"

[widgets.table-of-contents]
  after = ["changelog-toc-html", "other-toc-html"]
  widget = "toc"
  selector = "div#toc"
  heading_links = true
  heading_link_text = "→ "
  heading_link_class = "here"
  min_level = 2
  numbered_list = false
  toc_list_class = "toc"
  toc_class_levels = false
  use_heading_slug = true
  valid_html = true

[widgets.delete-readme-logo]
  widget = "delete-readme-img"
  page = ["index.html"]
  src_name = "./umo.png"

[widgets.prod-mode]
  after = [
    "highlight-active-link",
    "highlight-code-css",
    "highlight-code-darkmode-css"
  ]
  widget = "prod-mode"
  profile = "prod"
  project = "umo"

[widgets.highlight-active-link]
  widget = "section-link-highlight"
  selector = "nav#nav"
  active_link_class = "bold"

[widgets.tracking-js]
  profile = "prod"
  widget = "insert_html"
  html = '<script data-goatcounter="https://stats.gitlab.modding-openmw.com/count" async src="https://stats.gitlab.modding-openmw.com/count.js"></script>'
  selector = "body"
  parse = true

[widgets.highlight-code]
  widget = "preprocess_element"
  selector = '*[class^="language-"]'
  command = 'highlight -O html -f --syntax=$(echo $ATTR_CLASS | sed -e "s/language-//")'

[widgets.highlight-code-css]
  widget = "insert-if"
  html = "<link rel='stylesheet' type='text/css' href='/css/highlight.css' />"
  selector = "head"
  check_selector = "code"

[widgets.highlight-code-darkmode-css]
  widget = "insert-if"
  html = "<link rel='stylesheet' type='text/css' media='(prefers-color-scheme: dark)' href='/css/highlight-dark.css'/>"
  selector = "head"
  check_selector = "code"

# The HTML exported from org-mode includes a title that we don't need
[widgets.delete_org_title]
  widget = "delete_element"
  selector = "h1.title"
