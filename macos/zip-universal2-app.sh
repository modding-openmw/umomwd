#!/bin/bash

arm64_app=$(readlink -f $1)
x86_64_app=$(readlink -f $2)
result_app=$(readlink -f $3)
umo_path="Contents/MacOS/umo"
py_path="Contents/MacOS/Python"

cd $arm64_app

find -E . -regex '.*\.(dylib|so)' -type f | while read i
do
    echo lipo -create -output "$result_app/$i" "$x86_64_app/$i" "$arm64_app/$i"
    lipo -create -output "$result_app/$i" "$x86_64_app/$i" "$arm64_app/$i"
done

echo lipo -create -output "$result_app/$umo_path" "$x86_64_app/$umo_path" "$arm64_app/$umo_path"
lipo -create -output "$result_app/$umo_path" "$x86_64_app/$umo_path" "$arm64_app/$umo_path"

echo lipo -create -output "$result_app/$py_path" "$x86_64_app/$py_path" "$arm64_app/$py_path"
lipo -create -output "$result_app/$py_path" "$x86_64_app/$py_path" "$arm64_app/$py_path"

echo DONE